<?php
   $id = $receiver_user->id;
?>
@extends('layouts.admin-layout')

@section('content')
       
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
        <a href="/admin-index">Panel</a>
        </li>
        <li class="breadcrumb-item">
          <a href="/admin-messages">Mesajlarım</a>
        </li>
        <li class="breadcrumb-item active">{{$receiver_user->ad}} {{$receiver_user->soyad}}</li>
      </ol>
	  <!-- Icon Cards-->
      <div class="row">
        <div class="col-sm-12">
            <div id="live-chat">
                <header class="clearfix">
                   <h4>{{$receiver_user->ad}} {{$receiver_user->soyad}}</h4>
                   <span class="chat-message-counter">3</span>
                </header>
                <div class="chat">
                   <div class="chat-history" id="chat-history">
                      @foreach($messages as $message)
                        @if($user->id == $message->sender_id)
                           <div class="chat-message clearfix sender">
                              <img src="/uploads/{{$user->p_foto}}" width="32" height="32">
                              <div class="chat-message-content clearfix">
                                 <h5>{{ $user->ad }} {{ $user->soyad }}</h5>
                                 <span class="chat-time">{{$message->created_at}}</span>
                                 <p>{{$message->content}}</p>
                                 @if(!empty($message->file))
                                    <a class="message-file-link" href="/uploads/{{$message->file}}" target="_blank">
                                       <img src="/uploads/{{$message->file}}">
                                    </a>
                                 @endif
                              </div>
                           </div>
                        @else
                           <div class="chat-message clearfix">
                              <img src="/uploads/{{$receiver_user->p_foto}}" width="32" height="32">
                              <div class="chat-message-content clearfix">
                                 <h5>{{ $receiver_user->ad }} {{ $receiver_user->soyad }}</h5>
                                 <span class="chat-time">{{$message->created_at}}</span>
                                 <p>{{$message->content}}</p>
                                 @if(!empty($message->file))
                                    <a class="message-file-link" href="/uploads/{{$message->file}}" target="_blank">
                                       <img src="/uploads/{{$message->file}}">
                                    </a>
                                 @endif
                              </div>
                           </div>
                        @endif
                      @endforeach
                   </div>
                   <!-- end chat-history -->
                   {!! Form::open(['action' => 'MessageController@store' , 'id' => 'message-form' ,'method' => 'POST' , 'files' => 'true']) !!}
                      {{Form::text('Content','',['class' => 'form-control' , 'id' => 'message-content' , 'placeholder' => 'Mesajı giriniz...'])}}
                      <input type="hidden" name="sender_id" value="{{ $user->id }}">
                      <input type="hidden" name="receiver_id" value={{$receiver_user->id}}>
                      <label for="message-file" class="message-file-icon"><i class="fa fa-paperclip"></i></label>
                      <input name="message-file" type="file" id="message-file" hidden>
                      {{Form::submit('Gönder',['class' => 'btn_1 medium'])}}
                   {!! Form::close() !!}
                </div>
             </div>
             </div> 
        </div>
	  </div>
	  <!-- /.container-fluid-->
    </div>
    


@endsection

{{-- <script>
           setInterval(function () {
            axios.get('messages/show/1',)
                .then(function(response){
                        document.querySelector('#chat-history')
                                .innerHtml(response.data);
                }); 
            },1000); 

</script> --}}