@extends('layouts.danisan-admin-layout')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Panel</a>
        </li>
        <li class="breadcrumb-item active">Profil Düzenleme</li>
      </ol>
		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-user"></i>Profilini Düzenleyin</h2>
			</div>

			@include('common.errors')
			
			{!! Form::open(['action' => ['DanisanController@update' , $danisan->id], 'method' => 'POST', 'files'=>'true' ]) !!}
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Adınız</label>
							{{Form::text('Ad',$danisan->ad ,['class' => 'form-control' , 'placeholder' => 'Adınızı Giriniz'])}}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Soyadınız</label>
							{{Form::text('Soyad',$danisan->soyad ,['class' => 'form-control' , 'placeholder' => 'Soyadınızı Giriniz'])}}
					</div>
					</div>
				</div>
				<!-- /row-->
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Email Adresi</label>
							{{Form::text('Email',$danisan->email ,['class' => 'form-control' , 'placeholder' => 'Email Adresinizi Giriniz'])}}	
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Boy</label>
							{{Form::text('Boy',$danisan->boy ,['class' => 'form-control' , 'placeholder' => 'Boyunuzu Giriniz'])}}
						</div>
					</div>
				</div>
				<!-- /row-->
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Profil Fotoğrafınız</label>
							@if(!empty($danisan->p_foto))
								<img style="width: 100px; display: block; margin-bottom: 10px" src="/uploads/{{ $danisan->p_foto }}"/>
							@endif
							{{ Form::file('Profil',null)}}
						</div>
					</div>
				</div>
		</div>

			<div class="row">
				<div class="col-md-12">
					<div class="box_general padding_bottom">
						<div class="header_box version_2">
							<h2><i class="fa fa-lock"></i>Şifre Değiştirme</h2>
						</div>
						<div class="form-group">
							<label>Eski Şifre</label>
							{{Form::password('E_Sifre',['class' => 'form-control' , 'placeholder' => ''])}}
						</div>
						<div class="form-group">
							<label>Yeni Şifre</label>
							{{Form::password('Y_Sifre',['class' => 'form-control' , 'placeholder' => ''])}}
						</div>
						<div class="form-group">
							<label>Yeni Şifre Tekrar</label>
							{{Form::password('TY_Sifre',['class' => 'form-control' , 'placeholder' => ''])}}
						</div>
					</div>
				</div>			
			</div>

		{{Form::hidden('_method','PUT')}}
		<p>  {{Form::submit('Profilimi Düzenle',['class' => 'btn_1 medium'])}} </p>
			{!! Form::close() !!}
		</div>

		<!-- /box_general-->
		
		<!-- /row-->
		
		
	  </div>
	  <!-- /.container-fluid-->
   	</div>
	<!-- /.container-wrapper-->

@endsection