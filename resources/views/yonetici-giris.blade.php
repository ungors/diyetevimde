@extends('layouts.layout')

@section('title')
Yönetici Giriş Sayfası
@endsection

@section('content')
<main>
		<div class="bg_color_2">
			<div class="container margin_60_35">
				<div id="login-2">
					
						@include('common.errors')

					{!! Form::open(['action' => 'YonetimController@yonetici_login' , 'method' => 'POST']) !!}
						<div class="box_form clearfix">

							<div class="box_login last">
								<div class="form-group">
									{{Form::text('Kullanıcı','',['class' => 'form-control' , 'placeholder' => 'Kullanıcı Adınız'])}}
								</div>
								<div class="form-group">
									{{Form::password('Şifre', ['class' => 'form-control' , 'placeholder' => 'Şifreniz']) }}
									<a href="#0" class="forgot"><small>Şifrenizi unuttunuz mu?</small></a>
								</div>
								<div class="form-group text-center">
									{{Form::submit('Giriş Yap',['class' => 'btn_1 btn_login'])}}
									{{ Form::token() }}
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
				<!-- /login -->
			</div>
		</div>
</main>
	<!-- /main -->
@endsection
