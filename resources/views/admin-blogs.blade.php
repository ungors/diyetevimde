@extends('layouts.admin-layout')

@section('content')
    <!-- /Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/admin-index">Panel</a>
        </li>
        <li class="breadcrumb-item active">Bloglarım</li>
      </ol>
		<!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Bloglarım 
          <a href="{{URL('blogs/create')}}"><button style="float:right;" class="btn btn-primary">Blog Oluştur</button></a>
        </div>
          <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Başlık</th>
                  <th>İçerik</th>
                  <th>Resim</th>
                  <th>Oluşturma Tarihi</th>
                  <th>Düzenle</th>
                  <th>Sil</th>
                </tr>
              </thead>
              <tbody>

                @foreach($bloglar as $blog)
                <tr>
                  <td>{{$blog->baslik}}</td>
                  <td>{!! str_limit($blog->icerik, $limit = 250, $end = '...') !!}</td>
                  <td><img width="100px" src="uploads/{{$blog->resim}}"></td>
                  <td>{{$blog->created_at}}</td>
                <td> <a class="btn btn-success" href="{{ route('blogs.edit', $blog->slug) }}">Düzenle</a></td>
                <td>
                  {!! Form::open(['action' => ['BlogController@destroy' , $blog->id] , 'method' => 'POST' , 'class' => 'pull-right' ]) !!}
                  {{ Form::hidden('_method' , 'DELETE') }}
                  {{ Form::submit('Sil' , ['class' => 'btn btn-danger'])}}
                  {!! Form::close() !!}
                </td>
                </tr>
                @endforeach

              </tbody>
            </table>
            {{ $bloglar->links() }}
          </div>
        </div>
      </div>
	  <!-- /tables-->
	  </div>
	  <!-- /container-fluid-->
   	</div>
@endsection