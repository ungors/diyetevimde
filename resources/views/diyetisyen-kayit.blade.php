@extends('layouts.layout')

@section('title')
Diyetisyen Kayıt Sayfası
@endsection

@section('content')
<main>
		<div id="hero_register">
			<div class="container margin_120_95">			
				<div class="row justify-content-md-center">
					<!-- /col -->
					<div class="col-md-6">

							@include('common.errors')
							
						<div class="box_form">
		
							{!! Form::open(['action' => 'DiyetisyenController@store','method' =>'POST']) !!}
								
								<div class="row">
									<div class="col-md-6 ">
										<div class="form-group">
											{{Form::text('Name','',['class' => 'form-control' , 'placeholder' => 'Adınız'])}}
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											{{Form::text('LastName','',['class' => 'form-control' , 'placeholder' => 'Soyadınız'])}}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											{{Form::text('Email','',['class' => 'form-control' , 'placeholder' => 'E-posta adresiniz'])}}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											{{Form::password('Şifre',['class' => 'form-control' , 'placeholder' => 'Şifreniz'])}}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											{{Form::password('ŞifreTekrar',['class' => 'form-control' , 'placeholder' => 'Şifrenizin Tekrarı'])}}
										</div>
									</div>
								</div>
								<!-- /row -->
								<p class="text-center"> {{Form::submit('Kayıt Ol',['class' => 'btn_1 btn_register'])}}</p>
								{{ Form::token() }}
							{!! Form::close() !!}
						</div>
						<!-- /box_form -->
					</div>
					<!-- /col -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /hero_register -->
</main>
	<!-- /main -->
@endsection
