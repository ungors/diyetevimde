@extends('layouts.admin-layout')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admin-index">Panel</a>
        </li>
        <li class="breadcrumb-item active">Mesajlar</li>
        </ol>
        <div class="box_general">
            <h4>Mesaj Listesi</h4>
            <?php
                if(isset($_SESSION['diyetisyen']))
                {
                    $email = $_SESSION['diyetisyen'];
                    $user = DB::table('diyetisyens')->where('email',$_SESSION['diyetisyen'])->first();
                    $danisans = DB::table('danisans')->where('diyetisyen_id',$user->id)->paginate(5);
                    $message_count = 0;
                }
            ?>
            <div class="list_general">
                <ul>
                    @foreach($danisans as $danisan)
                    <?php
                        $message = DB::table('messages')
                        ->where([
                            ['sender_id', '=', $user->id],
                            ['receiver_id', '=', $danisan->id],
                        ])
                        ->orWhere([
                            ['receiver_id', '=', $user->id],
                            ['sender_id', '=', $danisan->id],
                        ])
                        ->latest('id')->first();

                        if($message)
                            $message_count++;
                    ?>
                        @if($message)
                        <li class="message-item">
                            <a href="{{ route('messages.show', $danisan->id) }}">
                                <span>{{ $message->created_at }}</span>
                                <figure><img src="/uploads/{{$danisan->p_foto}}" alt=""></figure>
                                <h4>{{$danisan->ad}} {{$danisan->soyad}}</h4>
                                <p>{{ $message->content }}</p>
                            </a>
                        </li>
                        @endif
                    @endforeach
                        @if($message_count == 0)
                            <li style="padding-left: 30px;padding-top: 15px; padding-bottom: 15px">
                                Herhangi bir mesajınız bulunmamaktadır.
                            </li>
                        @else
                        {{ $danisans->links() }}
                        @endif
                </ul>
            </div>
        </div>
    
        <!-- /box_general-->
        </div>
        <!-- /container-fluid-->
        </div>
    <!-- /container-wrapper-->

    @endsection