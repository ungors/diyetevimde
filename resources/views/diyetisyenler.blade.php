<?php 

	$tip = isset($type) ? $type : null;

?>

@extends('layouts.layout')

@section('title')
Diyetisyenler Sayfası
@endsection

@section('content')
<main class="theia-exception">
		<div id="results">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
					<h4>{{ count($lists) }} sonuç listelendi.</h4>
					</div> 
					<div class="col-md-6">
						
							@include('common.errors')

						{!! Form::open(['action' => 'DiyetisyenController@ara' , 'method' => 'POST']) !!}	
							<div class="search_bar_list">
								{{Form::text('Arama',isset($search) ? $arama : null,['class' => 'form-control' , 'placeholder' => 'Diyetisyen (Başak Öztürk)...'])}}
							
								{{Form::submit('Ara')}}
								</div>
							
						{!! Form::close() !!}
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /results -->

		<div class="filters_listing">
			<div class="container">
				<ul class="clearfix">
					<li>
						<h6>Sırala</h6>
						{!! Form::open(['action' => 'DiyetisyenController@sirala' , 'method' => 'POST']) !!}
							<select class="my-select" value="{{ $tip }}" name="orderby" class="selectbox" onchange="this.form.submit()">
								<option <?php echo $tip == "puan" ? "selected" : "" ?> value="puan">Puan</option>
								<option <?php echo $tip == "hizmet" ? "selected" : "" ?> value="hizmet">Hizmet Sayısı</option>
							</select>
						{!! Form::close() !!}
					</li>
				</ul>
			</div>
			<!-- /container -->
		</div>
		<!-- /filters -->
		
		<div class="container" style="padding: 20px 15px">
			<div class="row">
				<div class="col-lg-12">
					@if(count($lists) > 0)
						@foreach($lists as $list)
						<div class="strip_list wow fadeIn">
							{{-- <a href="#0" class="wish_bt"></a> --}}
							<figure>
							<img src="/uploads/{{$list->p_foto}}" alt="">
							</figure>
							<h3><a href="{{ route('diyetisyens.show', $list->slug) }}">{{$list->ad}} {{$list->soyad}}</a></h3>
			
							<p>{{ str_limit($list->bio, $limit = 250, $end = '...') }}</p>
						
							<span class="rating">
								@for($i = 0; $i < 5; $i++)
									@if($i < ceil($list->puan))
										<i class="icon_star voted"></i>
									@else
										<i class="icon_star"></i>
									@endif
								@endfor
								<small>{{$list->puan}}</small></span>
							<ul>
								<li></li>
								<li><a href="{{ route('diyetisyens.show', $list->slug) }}">Diyete Başla</a></li>
							</ul>
						</div>
						@endforeach
						
						{{$lists->links()}}
					@else
						<h2 style="text-align: center; margin-bottom: 30px">
							"{{$arama}}" kelimesine uygun diyetisyen bulunamadı. 
						</h2>
					@endif
					
					
				</div>
				<!-- /col -->
			
				
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</main>
	<!-- /main -->
   @endsection
