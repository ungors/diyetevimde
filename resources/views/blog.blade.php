@extends('layouts.layout')

@section('title')
Bloglar Sayfası
@endsection

@section('content')
<main>
    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="/index">Anasayfa</a></li>
                <li>Bloglar</li>
            </ul>
        </div>
    </div>
    <!-- /breadcrumb -->

    <div class="container margin_60">
        <div class="main_title">
                @if(isset($search))
                <h1>
                    Aradığınız Kelime: {{$arama}}
                </h1>
                <p>{{count($blogs)}} Sonuç Bulundu.</p>
                @else
                <h1>
                    Blog Yazılarımız
                </h1>
                <p>Uzman diyetisyenlerin çeşitli konular hakkındaki blogları</p>
                @endif
        </div>
        <div class="row">
            <div class="col-lg-9">
                @foreach($blogs as $blog)
                <article class="blog wow fadeIn">
                    <div class="row no-gutters">
                        <div class="col-lg-7">
                            <figure>
                                <a href="{{ route('blogs.show', $blog->slug) }}"><img src="uploads/{{$blog->resim}}" alt=""><div class="preview"><span>Devamını Göster</span></div></a>
                            </figure>
                        </div>
                        <div class="col-lg-5">
                            <div class="post_info">
                                <small>{{$blog->created_at->format('d/m/Y')}}</small>
                                <h3><a href="{{ route('blogs.show', $blog->slug) }}">{{$blog->baslik}}</a></h3>
                                {!! str_limit($blog->icerik, $limit = 250, $end = '...') !!}
                                <ul>
                                    <li>
                                        <div class="thumb"><img src="uploads/{{$blog->diyetisyen->p_foto}}" alt=""></div> {{$blog->diyetisyen->ad}} {{$blog->diyetisyen->soyad}}
                                    </li>
                                    <li style="padding-left: 115px; padding-right: 0"><i class="fa fa-eye" aria-hidden="true"></i> {{$blog->visit_count}}</li>
                                    <li><i style="vertical-align: top; display: inline-block; margin-top: 2px" class="icon_comment_alt"></i> {{count($blog->yorumlar)}} </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </article>
                @endforeach
                
            </div>
            <!-- /col -->
            
            <aside class="col-lg-3">
                <div class="widget">

                    @include('common.errors')
                        
                    {!! Form::open(['action' => 'BlogController@ara' , 'method' => 'POST']) !!}
                        <div class="form-group">
                            {{Form::text('Arama',isset($search) ? $arama : null,['class' => 'form-control' , 'placeholder' => 'Arama kelimesi giriniz.'])}}
                        </div>
                        {{Form::submit('Ara',['class' => 'btn_1 btn_md'])}}
                    {!! Form::close() !!}
                </div>
                <!-- /widget -->

                <div class="widget">
                    <div class="widget-title">
                        <h4>Yeni Bloglar</h4>
                    </div>
                    <ul class="comments-list">
                        @foreach($recents as $post)
                            <li>
                                <div class="alignleft">
                                    <a href="{{ route('blogs.show', $post->slug) }}"><img src="/uploads/{{$post->resim}}" alt=""></a>
                                </div>
                                <small>{{$post->created_at}}</small>
                                <h3><a href="{{ route('blogs.show', $post->slug) }}" title="">{!! str_limit($post->icerik, $limit = 40, $end = '...') !!}</a></h3>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- /widget -->
                
            </aside>
            <!-- /aside -->
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</main>
<!-- /main -->
@endsection
