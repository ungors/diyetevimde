@extends('layouts.danisan-admin-layout')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Panel</a>
        </li>
        <li class="breadcrumb-item active">Ölçü Girişi</li>
      </ol>
		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-user"></i>Ölçülerinizi Giriniz</h2>
			</div>
			
			@include('common.errors')

			{!! Form::open(['action' => 'DanisanController@olcuduzenle', 'method' => 'POST']) !!}
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Kilonuz</label>
							{{Form::text('Kilo',null,['class' => 'form-control' , 'placeholder' => 'Kilonuzu Giriniz'])}}
						</div>
					</div>		
				</div>		
		</div>
		<p>  {{Form::submit('Güncelle',['class' => 'btn_1 medium'])}} </p>
			{!! Form::close() !!}

	</div>
	  <!-- /.container-fluid-->
</div>
	<!-- /.container-wrapper-->
@endsection