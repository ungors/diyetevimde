@extends('layouts.yonetim-layout')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active">Hoşgeldiniz Yönetici</li>
      </ol>
		<!-- /cards -->
		<h2></h2>
		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-bar-chart"></i>Diyetisyenler</h2>
			</div>
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>Id</th>
								<th>Ad Soyad</th>
								<th>Mail</th>
								<th>Kayıt Tarihi</th>
								<th>Belge</th>
								<th>Onay Durumu</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($diyetisyenler as $diyetisyen)
									<tr>
									<td>{{$diyetisyen->id}}</td>
									<td>{{$diyetisyen->adsoyad}}</td>
									<td>{{$diyetisyen->email}}</td>
									<td>{{$diyetisyen->created_at}}</td>
									<td>
										@if(!empty($diyetisyen->belge))
										<a href="/uploads/{{$diyetisyen->belge}}" target="_blank"><img src="/uploads/{{$diyetisyen->belge}}" style="width: 90px"></a>
										@else
										Diyetisyenin belgesi bulunmamaktadır.
										@endif
									</td>
									<td>
										@if($diyetisyen->onay)
										<a class="btn btn-danger" href="/onayla/{{$diyetisyen->id}}">Onayı Kaldır</a>
										@else
											<a class="btn btn-success" href="/onayla/{{$diyetisyen->id}}">Onayla</a>
										@endif
									</td>
									</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
	  </div>
	  <!-- /.container-fluid-->
       </div>
       
@endsection