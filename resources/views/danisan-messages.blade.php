@extends('layouts.danisan-admin-layout')

@section('content')


<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/danisan-profile">Panel</a>
        </li>
        <li class="breadcrumb-item active">Mesajlar</li>
        </ol>
        <div class="box_general">
            <h4>Mesaj Listesi</h4>
            <?php
                if(isset($_SESSION['danisan']))
                {
                    $email = $_SESSION['danisan'];
                    $user = DB::table('danisans')->where('email',$email)->first();
                    $message_count = 0;
                }
            ?>
            <div class="list_general">
                <ul>
                    @foreach($diyetisyens as $diyetisyen)
                    <?php
                        $message = DB::table('messages')
                        ->where([
                            ['sender_id', '=', $user->id],
                            ['receiver_id', '=', $diyetisyen->id],
                        ])
                        ->orWhere([
                            ['receiver_id', '=', $user->id],
                            ['sender_id', '=', $diyetisyen->id],
                        ])
                        ->latest('id')->first();
                            if($message)
                                $message_count++;
                    ?>
                        @if($message)
                        <li class="message-item">
                        <a href="{{ route('messages.show', $diyetisyen->id) }}">
                                <span>{{ $message->created_at }}</span>
                                <figure><img src="/uploads/{{$diyetisyen->p_foto}}" alt=""></figure>
                                <h4>{{$diyetisyen->ad}} {{$diyetisyen->soyad}}</h4>
                                <p>{{ $message->content }}</p>
                            </a>
                        </li>
                        @endif
                    @endforeach
                    @if($message_count == 0)
                    <li style="padding-left: 30px;padding-top: 15px; padding-bottom: 15px">
                            Herhangi bir mesajınız bulunmamaktadır.
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    
    </div>
        <!-- /container-fluid-->
</div>
    <!-- /container-wrapper-->
@endsection