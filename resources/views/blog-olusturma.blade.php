@extends('layouts.admin-layout')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/admin-index">Panel</a>
        </li>
        <li class="breadcrumb-item active">Blog Yazısı Oluştur</li>
      </ol>
      <div class="box_general padding_bottom">
        <div class="header_box version_2">
          <h2><i class="fa fa-file"></i>Blog Yazısı Oluştur</h2>
        </div>

        @include('common.errors')
        
        {!! Form::open(['action' => 'BlogController@store' , 'method' => 'POST' , 'files'=>'true']) !!}
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Blog Başlığı</label>
              {{Form::text('Baslik','',['class' => 'form-control' , 'placeholder' => 'Blog Başlığı'])}}
            </div>
          </div>
        </div>
        <!-- /row-->
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Blog İçeriği</label>
              {{ Form::textarea('İcerik',null,['id' => 'article-ckeditor','class'=>'form-control', 'rows' => 5 , 'placeholder' => 'Blog İçeriği Giriniz...']) }}
            </div>
          </div>
        </div>
        <!-- /row-->
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Blog Resmi</label>
              <br>
              {{ Form::file('Resim',null)}}
            </div>
          </div>
        </div>
        <!-- /row-->
      </div>
      <p>  {{Form::submit('Oluştur',['class' => 'btn_1 medium'])}} </p>
      {!! Form::close() !!}

    </div>
    <!-- /.container-fluid-->
  </div>
  <!-- /.container-wrapper-->

@endsection