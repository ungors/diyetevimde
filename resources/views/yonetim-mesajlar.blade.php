@extends('layouts.yonetim-layout')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active">Hoşgeldiniz Yönetici</li>
      </ol>
		<!-- /cards -->
		<h2></h2>
		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-bar-chart"></i>Mesajlar</h2>
			</div>
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>Id</th>
								<th>Ad</th>
								<th>Soyad</th>
								<th>Telefon</th>
								<th>Mail</th>
                                <th>Mesaj</th>
                                <th>Mesaj Tarihi</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($mesajlar as $mesaj)
									<tr>
									<td>{{$mesaj->id}}</td>
									<td>{{$mesaj->ad}}</td>
									<td>{{$mesaj->soyad}}</td>
									<td>{{$mesaj->telefon}}</td>
                                    <td> {{$mesaj->email}}</td>
                                    <td> {{$mesaj->mesaj }} </td>
									<td> {{$mesaj->created_at}} </td>
									</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
	  </div>
	  <!-- /.container-fluid-->
       </div>
       
@endsection