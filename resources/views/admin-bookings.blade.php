 <?php 
	use App\Danisan;
	use App\Diyetisyen;
	use App\Odeme;

	ob_start();
	session_start();

	$email = $_SESSION["diyetisyen"];
	$user = Diyetisyen::where("email", $email)->first();	
 ?>
 
 @extends('layouts.admin-layout')

 @section('content')

 <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/admin-index">Panel</a>
        </li>
        <li class="breadcrumb-item active">Danışanlarım</li>
      </ol>
		<div class="box_general">
			<div class="header_box">
				<h2 class="d-inline-block">Danışan Listesi</h2>
			</div>
			<div class="list_general">
				@if(count(Danisan::where("diyetisyen_id", $user->id)->get()) > 0)
				<ul>
					@foreach(Danisan::where("diyetisyen_id", $user->id)->get() as $danisan)
					<li>
						<?php
							 $odeme_detay = Odeme::where([
								 ["diyetisyen_id", "=", $user->id],
								 ["danisan_id", "=", $danisan->id]
							 ])->latest("id")->first();
							 

							 $bitis_tarihi = strtotime($odeme_detay->bitis_tarihi) - time();

 							 $bitis_tarihi = floor($bitis_tarihi / (60*60*24) );
						?>
						<figure><img src="/uploads/{{$danisan->p_foto}}"></figure>
						<h4>{{$danisan->ad}} {{$danisan->soyad}} <i class="pending">{{$bitis_tarihi}} Günü Kaldı.</i></h4>
						<ul class="booking_details">
							<li><strong>Hizmet Süresi / Ücret:</strong> {{$odeme_detay->ay}} Ay - {{$odeme_detay->ucret}} ₺</li>
							<li><strong>Telefon:</strong> {{$danisan->telefon}}</li>
							<li><strong>Email:</strong> {{$danisan->email}}</li>
						</ul>
						<ul class="buttons">
							<li><a href="/messages/{{$danisan->id}}" class="btn_1 gray approve"><i class="fa fa-fw fa-envelope-o"></i> Mesajlara Git</a></li>						</ul>
					</li>
					@endforeach
				</ul>
				@else
					<ul>
						<li style="padding: 15px 30px">Herhangi bir danışanınız bulunmamaktadır.</li>
					</ul>
				@endif
			</div>
		</div>
		<!-- /pagination-->
	  </div>
	  <!-- /container-fluid-->
   	</div>


 @endsection