@extends('layouts.layout')

@section('title')
Blog Detay Sayfası
@endsection

@section('content')
<main>
		<div id="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="/index">Anasayfa</a></li>
					<li><a href="/blogs">Bloglar</a></li>
					<li>{{$detay->baslik}}</li>
				</ul>
			</div>
		</div>
		<!-- /breadcrumb -->
		
		<div class="container margin_60">
			<div class="row">
				<div class="col-lg-9">
					<div class="bloglist singlepost">
						<p><img class="img-fluid" src="/uploads/{{$detay->resim}}"></p>
						<h1>{{$detay->baslik}}</h1>
						<div class="postmeta">
							<ul>
								<li><i class="icon_clock_alt"></i> Yayın Tarihi: {{$detay->created_at}}</li>
								<li><a href="/diyetisyens/{{$detay->diyetisyen->slug}}"><i class="icon_pencil-edit"></i> {{$detay->diyetisyen->ad}} {{$detay->diyetisyen->soyad}}</a></li>
								<li><a href="#comments"><i class="icon_comment_alt"></i> ({{count($detay->yorumlar)}}) Yorum</a></li>
								<li><i class="fa fa-eye"></i> {{$detay->visit_count}}</li>
							</ul>
						</div>
						<!-- /post meta -->
						<div class="post-content">
							<div class="dropcaps">
								{!! $detay->icerik !!}
							</div>
						</div>
						<!-- /post -->
					</div>
					<!-- /single-post -->
					<?php
						$id = $detay->id;
					?>

					<div id="comments">
						@if(isset($_SESSION['danisan']))
						<h5>Yorum Ekle</h5>

						@include('common.errors')

						{!! Form::open(['action' => ['YorumController@store', $id], 'method' => 'POST']) !!}
							<div class="form-group">
								{{ Form::textarea('Yorum',null,['class'=>'form-control', 'rows' => 6, 'cols' => 40, 'placeholder' => 'Yorumunuzu Giriniz']) }}
							</div>
							<div class="form-group">
							<input type="hidden" name="blog_id" value="<?php echo $id; ?>" />
							{{ Form::submit('Yorum Ekle',['class' => 'btn_1']) }}
							</div>
						{!! Form::close() !!}
						@endif
						<hr>
						<?php use App\Danisan; ?>
						<h5>Yorumlar ({{count($detay->yorumlar)}})</h5>
						<ul>

							@foreach ($detay->yorumlar as $yorum)

							<?php 
								$user = Danisan::find($yorum->danisan_id);	
							?>
							<li>
								<div class="avatar">
									<a href="#"><img src="/uploads/{{$user->p_foto}}" alt="">
									</a>
								</div>
								<div class="comment_right clearfix">
									<div class="comment_info">
										Yorum Sahibi: <b>{{$user->ad}} {{$user->soyad}}</b><span>|</span>{{$yorum->created_at}}<span>
									</div>
									<p>
										{{$yorum->icerik}}
									</p>
								</div>
							</li>

							@endforeach
						</ul>
					</div>
				</div>
				<!-- /col -->
				
				<aside class="col-lg-3">
					<div class="widget">
						
							@include('common.errors')

							{!! Form::open(['action' => 'BlogController@ara' , 'method' => 'POST']) !!}
								<div class="form-group">
									{{Form::text('Arama',isset($search) ? $arama : null,['class' => 'form-control' , 'placeholder' => 'Arama kelimesi giriniz.'])}}
								</div>
								{{Form::submit('Ara',['class' => 'btn_1 btn_md'])}}
							{!! Form::close() !!}
						</div>
						<!-- /widget -->
		
						<div class="widget">
							<div class="widget-title">
								<h4>Yeni Bloglar</h4>
							</div>
							<ul class="comments-list">
								@foreach($recents as $post)
									<li>
										<div class="alignleft">
											<a href="{{ route('blogs.show', $post->id) }}"><img src="/uploads/{{$post->resim}}" alt=""></a>
										</div>
										<small>{{$post->created_at}}</small>
										<h3><a href="{{ route('blogs.show', $post->id) }}" title="">{!! str_limit($post->icerik, $limit = 40, $end = '...') !!}</a></h3>
									</li>
								@endforeach
							</ul>
						</div>
						<!-- /widget -->
				</aside>
				<!-- /aside -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
</main>
	<!-- /main -->
@endsection
