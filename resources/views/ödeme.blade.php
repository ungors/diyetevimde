@extends('layouts.layout')

@section('title')
Ödeme Sayfası
@endsection


@section('content')
<main>
		<div id="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">Category</a></li>
					<li>Page active</li>
				</ul>
			</div>
		</div>
		<!-- /breadcrumb -->

		<div class="container margin_60">
			<div class="row">
				<div class="col-xl-8 col-lg-8">
				<div class="box_general_3 cart">

					<div class="form_title">
						<h3><strong>2</strong>Ödeme Bilgileri</h3>
						<p>
							Lütfen kart bilgilerinizi giriniz.
						</p>
					</div>
					{!! Form::open(['action' => 'OdemeController@store' , 'method' => 'POST' , 'id' => 'payment-form']) !!}
					<div class="step">
						<div id="charge-error" class="alert alert-danger" style="visibility:hidden"></div>
						<div class="row">
							<input type="hidden" value()>
							<div class="col-md-6">
								<div class="form-group">
									<label>Card number</label>
									{{Form::text('card_number','',['class' => 'form-control', 'id' => 'card_number' , 'placeholder' => 'xxxx - xxxx - xxxx - xxxx']) }}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Expiration date</label>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											{{Form::text('expire_month','',['class' => 'form-control', 'id' => 'expire_month' , 'placeholder' => 'MM']) }}
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											{{Form::text('expire_year','',['class' => 'form-control', 'id' => 'expire_year' , 'placeholder' => 'Year']) }}
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Security code</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												{{Form::text('ccv','',['class' => 'form-control', 'id' => 'ccv' , 'placeholder' => 'CCV']) }}
											</div>
										</div>
									</div>
								</div>
							</div>
							{{Form::submit('Ödeme Yap',['class' => 'btn_1 btn_register'])}}
						</div>
						<!--End row -->
					
					<!--End step -->
				</div>
				{!! Form::close() !!}
					
					<hr>
					<!--End step -->
				</div>
				</div>
				<!-- /col -->
				
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
</main>
	<!-- /main -->
@endsection

	