<?php 
use Illuminate\Support\Facades\DB;
use App\Danisan;
use App\Diyetisyen;

ob_start();

	if(!isset($_SESSION)) 
	{ 
		session_start(); 
	} 

?>
<!DOCTYPE html>
<html lang="tr">
	
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Find easily a doctor and book online an appointment">
	<meta name="author" content="Ansonika">
	<title> @yield('title') </title>

	<!-- Favicons-->
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

	<!-- BASE CSS -->
	
	<link rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
	<link rel="stylesheet" href="{{URL::asset('css/menu.css')}}">
	<link rel="stylesheet" href="{{URL::asset('css/vendors.css')}}">
	<link rel="stylesheet" href="{{URL::asset('css/blog.css')}}">
	<link rel="stylesheet" href="{{URL::asset('css/date_picker.css')}}">
	<link href="{{URL::asset('css/chat.css')}}" rel="stylesheet">
	<link href="{{URL::asset('css/all_icons_min.css')}}" rel="stylesheet">

	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
	
	<div id="preloader" class="Fixed">
		<div data-loader="circle-side"></div>
	</div>
	<!-- /Preload-->
	
	<div id="page">		
	<header class="header_sticky">	
		<a href="#menu" class="btn_mobile">
			<div class="hamburger hamburger--spin" id="hamburger">
				<div class="hamburger-box">
					<div class="hamburger-inner"></div>
				</div>
			</div>
		</a>
        
		<!-- /btn_mobile-->
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-6">
					<div id="logo_home">
					<h1><a href="{{URL('/index')}}" title="Findoctor">DiyetEvimde</a></h1>
					</div>
				</div>
				<div class="col-lg-9 col-6">
					<nav id="menu" class="main-menu">
						<ul>
							<li><span><a href="{{URL('index')}}">Anasayfa</a></span></li>
							<li><span><a href="{{URL('diyetisyens/')}}">Diyetisyenler</a></span></li>
							<li><span><a href="{{URL('blogs/')}}">Blog</a></span></li>
							<li><span><a href="{{URL('iletisim')}}">İletişim</a></span></li>
							@if(isset($_SESSION['danisan']))
								<li>
									<?php 
										$email = $_SESSION['danisan'];
										$danisan = DB::table('danisans')->where('email',$email)->first(); 
									?>
									<span><a href="#">Hoşgeldin <?php echo $danisan->ad; ?></a></span>
									<ul>
										<li><a href="{{URL('danisan-profile')}}">Danışan Paneli</a></li>
										<li><a href="{{URL('danisan_cikis')}}">Çıkış Yap</a></li>
									</ul>
								</li>
							@elseif(isset($_SESSION['diyetisyen']))
								<li>
									<?php 
										$email = $_SESSION['diyetisyen'];
										$diyetisyen = DB::table('diyetisyens')->where('email',$email)->first(); 
									?>
									<span><a href="#">Hoşgeldin <?php echo $diyetisyen->ad; ?></a></span>
									<ul>
										<li><a href="{{URL('admin-index')}}">Diyetisyen Paneli</a></li>
										<li><a href="{{URL('cikis')}}">Çıkış Yap</a></li>
									</ul>
								</li>
							@else
								<li>
									<span><a href="{{URL('giris')}}"><i class="fa fa-user-o"></i></a></span>
									<ul>
										<li><a href="{{URL('giris')}}">Diyetisyen Giriş</a></li>
										<li><a href="{{URL('danisan-giris')}}">Danışan Giriş</a></li>
									</ul>
								</li>
								<li>
									<span><a href="#0"><i class="fa fa-user-plus"></i></a></span>
									<ul>
										<li><a href="{{URL('diyetisyen-kayit')}}">Diyetisyen Kayıt</a></li>
										<li><a href="{{URL('/danisan-kayit')}}">Danışan Kayıt</a></li>
									</ul>
								</li>
							@endif
						</ul>
					</nav>
					<!-- /main-menu -->
				</div>
			</div>
		</div>
		<!-- /container -->
    </header>
    
    @yield('content')

    <footer>
		<div class="container margin_60_35" style="padding-bottom: 20px">
			<div class="row">
				<div class="col-lg-3 col-md-12">
					<p>
					<a class="footer-logo" href="{{URL('/index')}}" title="Findoctor">
							<img src="img/logo.png" data-retina="true" alt="" width="40" height="36">
							<span>DiyetEvimde</span>
						</a>
					</p>
				</div>
				<div class="col-lg-3 col-md-4">
					<h5>Faydalı Linkler</h5>
					<ul class="links">
						<li><a href="{{URL('index')}}">Anasayfa</a></li>
						<li><a href="{{URL('diyetisyens/')}}">Diyetisyenler</a></li>
						<li><a href="{{URL('blog')}}">Blog</a></li>
						<li><a href="{{URL('iletisim')}}">İletişim</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-4">
					<h5>İşlemler</h5>
					<ul class="links">
						<li><a href="{{URL('giris')}}">Giriş Yap</a></li>
						<li><a href="{{URL('diyetisyen-kayit')}}">Kayıt Ol</a></li>
						<li><a href="#">Uygulamayı İndir</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-4">
					<h5>Bizimle İletişime Geçin</h5>
					<ul class="contacts">
						<li><a href="tel://61280932400"><i class="fa fa-phone"></i> 0(232) 312 45 98</a></li>
						<li><a href="mailto:info@findoctor.com"><i class="fa fa-envelope"></i> destek@diyetevimde.com</a></li>
					</ul>
					<div class="follow_us">
						<h5>Bizi Takip Edin</h5>
						<ul>
							<li><a href="#0"><i style="color: #3b5999" class="fa fa-facebook"></i></a></li>
							<li><a href="#0"><i style="color: #55acee" class="fa fa-twitter"></i></a></li>
							<li><a href="#0"><i style="color: #0077B5" class="fa fa-linkedin"></i></a></li>
							<li><a href="#0"><i style="color: #e4405f" class="fa fa-instagram"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<!--/row-->
			<hr>
			<div class="row">
				<div class="col-md-8">
					<ul id="additional_links">
						<li><a href="#0">Gizlilik ve Güvenlik</a></li>
					</ul>
				</div>
				<div class="col-md-4">
					<div id="copy">© 2018 DiyetEvimde</div>
				</div>
			</div>
		</div>
	</footer>
	<!--/footer-->
	</div>
	<!-- page -->

	<div id="toTop"></div>
	<!-- Back to top button -->

	

	<!-- COMMON SCRIPTS -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://js.stripe.com/v3/"></script>
	<script src="{{URL::asset('js/common_scripts.min.js')}}"></script>
	<script src="{{URL::asset('js/functions.js')}}"></script>
	<script src="{{URL::asset('js/charge.js')}}"></script>

	@yield('script')

	<script>

		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip(); 
	
			$('#live-chat header').on('click', function() {
	
				$('.chat').slideToggle(300, 'swing');
				$('.chat-message-counter').fadeToggle(300, 'swing');
	
			});
	
			$('.chat-close').on('click', function(e) {
	
				e.preventDefault();
				$('#live-chat').fadeOut(300);
	
			});
	
			$(".message-item").click(function() {
				$("#live-chat").fadeIn("fast")
			})

			$(".price-select").change(function() {
				$(".price span").text(($(this).val() * $(".d-ucret").text()) + "₺")
				$("#ucret").val(($(this).val() * $(".d-ucret").text()))
			})
		});

		
	</script>

</body>

</html>