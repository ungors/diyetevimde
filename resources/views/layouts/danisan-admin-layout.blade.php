<?php 
use App\Message; 
use Illuminate\Support\Facades\DB;
use App\Danisan;
use App\Diyetisyen; 
use App\VucutOlcusu; 

ob_start();

  if(!isset($_SESSION)) 
  { 
      session_start(); 
  } 

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Ansonika">
  <title>diyetevimde.com - Panel</title>
	
  <!-- Favicons-->
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">
	
  <!-- Bootstrap core CSS-->
  <link rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}">
  <!-- Icon fonts-->
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <!-- Plugin styles -->
  <link href="{{URL::asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
  <!-- Main styles -->
  <link href="{{URL::asset('css/admin.css')}}" rel="stylesheet">
  <link href="{{URL::asset('css/chat.css')}}" rel="stylesheet">
  <!-- Your custom styles -->
  {{-- <link href="css/admin.css" rel="stylesheet"> --}}
	<script src="{{URL::asset('vendor/jquery/jquery.min.js')}}"></script>
</head>

<body class="fixed-nav sticky-footer" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-default fixed-top" id="mainNav">
    <a class="navbar-brand" href="{{URL('index')}}"><img src="/img/logo.png" data-retina="true" alt="" width="40" height="36"><span>DiyetEvimde</span></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
        <a class="nav-link" href="{{URL('danisan-profile')}}">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Panel</span>
          </a>
        </li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Messages">
          <a class="nav-link" href="{{URL('danisan-messages')}}">
            <i class="fa fa-fw fa-envelope-open"></i>
            <span class="nav-link-text">Mesajlarım</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Add listing">
          <a class="nav-link" href="{{URL('danisan-edit')}}">
            <i class="fa fa-fw fa-user"></i>
            <span class="nav-link-text">Profilim</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Add listing">
            <a class="nav-link" href="{{URL('danisan-olculeri')}}">
              <i class="fa fa-fw fa-list"></i>
              <span class="nav-link-text">Ölçülerim</span>
            </a>
          </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Add listing">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-user-times"></i>
            <span class="nav-link-text">Çıkış Yap</a></span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-envelope"></i>
            <span class="d-lg-none">Messages
              <span class="badge badge-pill badge-primary">12 New</span>
            </span>
            <span class="indicator text-primary d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
          </a>
          <div class="dropdown-menu" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">Mesajlarım:</h6>
            <div class="dropdown-divider"></div>
            <?php 
              $user = Danisan::where('email',$_SESSION['danisan'])->first();
              $diyetisyens = Diyetisyen::where('id',$user->diyetisyen_id)->get();           
              $message_count = 0;   
            ?>
            @foreach($diyetisyens as $diyetisyen)
            <?php
                $message = DB::table('messages')
                ->where([
                    ['sender_id', '=', $user->id],
                    ['receiver_id', '=', $diyetisyen->id],
                ])
                ->orWhere([
                    ['receiver_id', '=', $user->id],
                    ['sender_id', '=', $diyetisyen->id],
                ])
                ->latest('id')->first();

                  if($message)
                      $message_count++;
            ?>
            
              @if($message)
                <a class="dropdown-item" href="/messages/{{$diyetisyen->id}}">
                <img class="dropdown-img" src="/uploads/{{$diyetisyen->p_foto}}">
                  <div class="dropdown-item-inner">
                    <strong>{{$diyetisyen->ad}} {{$diyetisyen->soyad}}</strong>
                    <span style="margin-left: 15px" class="small text-muted">{{$message->created_at}}</span>
                    <div class="clearfix"></div>
                    <div style="margin-top: 10px" class="dropdown-message small">{{$message->content}}</div>
                  </div>
                </a>
              <div class="dropdown-divider"></div>
              @endif
            @endforeach

            @if($message_count > 0)
            <a class="dropdown-item small" href="/danisan-messages">Tüm Mesajları Görüntile</a>
            @else
            <span class="dropdown-item small">Görüntülenecek Mesajınız Yoktur.</span>
            @endif
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Çıkış Yap</a>
        </li>
      </ul>
    </div>
  </nav>
  

  @yield('content')

    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Diyetevimde 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Gerçekten Çıkmak İstiyor musunuz?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Çıkmak için "Çıkış Yap" butonuna tıklayın.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">İptal Et</button>
          <a class="btn btn-primary" href="{{URL('/danisan_cikis')}}">Çıkış Yap</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="{{URL::asset('vendor/jquery/jquery.min.js')}}"></script>
    <script>
      
    </script>
    <script src="{{URL::asset('vendor/bootstrap.bundle.min.js')}}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{URL::asset('vendor/jquery/jquery.easing.min.js')}}"></script>
    <!-- Page level plugin JavaScript-->
    <script src="{{URL::asset('vendor/Chart.js')}}"></script>
    <script src="{{URL::asset('vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{URL::asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
    <script src="{{URL::asset('vendor/jquery/jquery.selectbox-0.2.js')}}"></script>

    <script src="{{URL::asset('vendor/retina-replace.min.js')}}"></script>
    <script src="{{URL::asset('vendor/jquery/jquery.magnific-popup.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{URL::asset('js/admin.js')}}"></script>
	<!-- Custom scripts for this page-->
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>

  <script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        
        $(".chat-history").scrollTop($('.chat-history').prop("scrollHeight"))
    });
  </script>

  <?php
  
    $olculer = VucutOlcusu::where("danisan_id", $user->id)->orderBy('created_at', 'asc')->get();

    $tarih = [];

    $kilo = [];


    foreach($olculer as $olcu) {
       array_push($tarih, $olcu->created_at->format('d.m.Y'));
       array_push($kilo, $olcu->kilo);
    }

  ?>

  <script>
    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#292b2c';
    // -- Area Chart Example
    var ctx = document.getElementById("myAreaChart");
    var myLineChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: <?php echo json_encode($tarih); ?>,
        datasets: [{
          label: "Kilo",
          lineTension: 0.3,
          backgroundColor: "rgba(2,117,216,0.2)",
          borderColor: "rgba(2,117,216,1)",
          pointRadius: 5,
          pointBackgroundColor: "rgba(2,117,216,1)",
          pointBorderColor: "rgba(255,255,255,0.8)",
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgba(2,117,216,1)",
          pointHitRadius: 20,
          pointBorderWidth: 2,
          data: <?php echo json_encode($kilo); ?>,
        }],
      },
      options: {
        scales: {
          xAxes: [{
            time: {
              unit: 'date'
            },
            gridLines: {
              display: false
            },
            ticks: {
              maxTicksLimit: 7
            }
          }],
          yAxes: [{
            ticks: {
              min: 0,
              max: 200,
              maxTicksLimit: 5
            },
            gridLines: {
              color: "rgba(0, 0, 0, .125)",
            }
          }],
        },
        legend: {
          display: false
        }
      }
    });

    

  </script>
	
</body>
</html>
