 @extends('layouts.admin-layout')

 @section('content')
  <!-- /Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/admin-index">Panel</a>
        </li>
        <li class="breadcrumb-item active">Profil Düzenle</li>
	  </ol>

	  @include('common.errors')
	   
	  {!! Form::open(['action' => ['DiyetisyenController@update' , $diyetisyen->id], 'method' => 'POST' , 'files'=>'true']) !!}
		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-user-md"></i>Uzman Bilgileri</h2>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Ad</label>
						{{Form::text('Ad',$diyetisyen->ad ,['class' => 'form-control' , 'placeholder' => 'Adınızı Giriniz'])}}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Soyadınız</label>
						{{Form::text('Soyad',$diyetisyen->soyad ,['class' => 'form-control' , 'placeholder' => 'Soyadınızı Giriniz'])}}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Ünvan</label>
						{{Form::text('Unvan',$diyetisyen->unvan ,['class' => 'form-control' , 'placeholder' => 'Ünvanınızı Giriniz: Dr. Dyt. Uzm.'])}}
					</div>
				</div>
			</div>
					
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Kişisel Telefon</label>
						{{Form::text('K_Telefon',$diyetisyen->telefon ,['class' => 'form-control' , 'placeholder' => 'Kişisel Telefonunuzu Giriniz'])}}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Ofis Telefonu</label>
						{{Form::text('O_Telefon',$diyetisyen->ofis_telefon ,['class' => 'form-control' , 'placeholder' => 'Ofis Telefonunuzu Giriniz'])}}
					</div>
				</div>
			</div>
			<!-- /row-->
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Email Adresi</label>
						{{Form::text('Email',$diyetisyen->email ,['class' => 'form-control' , 'placeholder' => 'Email Adresinizi Giriniz'])}}	
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Hakkımda</label>
						{{ Form::textarea('Hakkimda',$diyetisyen->bio,['class'=>'form-control', 'rows' => 5, 'cols' => 40, 'placeholder' => 'Kendinizi Anlatan Kısa Bir Yazı Yazınız.']) }}

					</div>
				</div>
			</div>
			<!-- /row-->
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label style="display: block">Profile Resminiz</label>
						@if(!empty($diyetisyen->p_foto))
							<img style="width: 100px; display: block; margin-bottom: 10px" src="/uploads/{{ $diyetisyen->p_foto }}"/>
						@endif
						{{ Form::file('Profil',null)}}
					</div>
				</div>
			</div>
			<!-- /row-->
		</div>
		<!-- /box_general-->
		
		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-graduation-cap"></i>Eğitim & Hizmet Bilgileri</h2>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Eğitim Bilgileriniz</label>
						{{ Form::textarea('Egitim',$diyetisyen->egitim,['class'=>'form-control', 'rows' => 5, 'cols' => 40, 'placeholder' => 'Eğitim Aldığınız Kurumları ve Bölümleri Alt Alta Yazınız.']) }}
					</div>
				</div>
			</div>
			<!-- /row-->
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Verdiğiniz Hizmetler</label>
						{{Form::text('Hizmetler',$diyetisyen->hizmetler,['class' => 'form-control' , 'placeholder' => 'Verdiğiniz Hizmetleri Virgül İle Ayırarak Giriniz'])}}	
					</div>
				</div>
			</div>
			<!-- /row-->
			<div class="row" style="margin-top: 20px">
				<div class="col-md-12">
					<div class="form-group">
						<label style="display: block">Uzmanlığınızı belgeleyecek bir döküman ekleyiniz. <i style="margin-left: 7px; color: #8b8b8b; cursor: pointer; font-size: 15px" class="fa fa-info-circle" data-toggle="tooltip" title="Hizmet verebilmeniz ve sizi onaylayabilmemiz için uzmanlığınızı belgeleyecek bir döküman paylaşmanız gereklidir."></i></label>
						@if(!empty($diyetisyen->belge))
							<img style="width: 100px; display: block; margin-bottom: 10px" src="/uploads/{{ $diyetisyen->belge }}"/>
						@endif
						{{ Form::file('Belge',null)}}
					</div>
				</div>
			</div>
		</div> 
		<!-- /box_general-->
				
		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-try"></i>Ücretlendirme</h2>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h6>Aylık Ücret Belirleyiniz</h6>
					<table id="pricing-list-container" style="width:100%;">
						<tr class="pricing-list-item">
							<td>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											{{Form::text('Ucret',$diyetisyen->ucret,['class' => 'form-control' , 'placeholder' => 'Ücret'])}}	
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<!-- /row-->
		</div>
		<!-- /box_general-->
		<div class="row">
			<div class="col-md-12">
				<div class="box_general padding_bottom">
					<div class="header_box version_2">
						<h2><i class="fa fa-lock"></i>Şifre Değiştirme</h2>
					</div>
					<div class="form-group">
						<label>Eski Şifre</label>
						{{Form::password('E_Sifre',['class' => 'form-control' , 'placeholder' => ''])}}
					</div>
					<div class="form-group">
						<label>Yeni Şifre</label>
						{{Form::password('Y_Sifre',['class' => 'form-control' , 'placeholder' => ''])}}
					</div>
					<div class="form-group">
						<label>Yeni Şifre Tekrar</label>
						{{Form::password('TY_Sifre',['class' => 'form-control' , 'placeholder' => ''])}}
					</div>
				</div>
			</div>			
		</div>

		{{Form::hidden('_method','PUT')}}
		<p>  {{Form::submit('Düzenle',['class' => 'btn_1 medium'])}} </p>
		{!! Form::close() !!}
	  </div>
	  <!-- /.container-fluid-->
   	</div>
    <!-- /.container-wrapper-->
   
    @endsection