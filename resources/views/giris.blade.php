@extends('layouts.layout')

@section('title')
Diyetisyen Giriş Sayfası
@endsection

@section('content')
<main>
		<div class="bg_color_2">
			<div class="container margin_60_35">
				<div id="login-2">
					
						@include('common.errors')

					{!! Form::open(['action' => 'DiyetisyenController@login' , 'method' => 'POST']) !!}
						<div class="box_form clearfix">

							<div class="box_login last">
								<div class="form-group">
									{{Form::text('Email','',['class' => 'form-control' , 'placeholder' => 'E-posta adresiniz'])}}
								</div>
								<div class="form-group">
									{{Form::password('Şifre', ['class' => 'form-control' , 'placeholder' => 'Şifreniz']) }}
									<a href="#0" class="forgot"><small>Şifrenizi unuttunuz mu?</small></a>
								</div>
								<div class="form-group text-center">
									{{Form::submit('Giriş Yap',['class' => 'btn_1 btn_login'])}}
									{{ Form::token() }}
								</div>
							</div>
						</div>
					{!! Form::close() !!}
					<p class="text-center link_bright">Hala bir üyeliğiniz yok mu? <a href="{{URL('diyetisyen-kayit')}}"><strong>Kayıt ol!</strong></a></p>
				</div>
				<!-- /login -->
			</div>
		</div>
</main>
	<!-- /main -->
@endsection
