<?php
  use App\VucutOlcusu;
  use App\Odeme;
  
  $item = VucutOlcusu::where('danisan_id',$danisan->id)->latest('id')->first();

    if(isset($item))
    {
        $vki  = $item->kilo / pow($danisan->boy/100,2);
    }

  $hizmet = Odeme::where("danisan_id", $danisan->id)->latest("id")->first();
  
    if(isset($hizmet))
    {
        $bitis_tarihi = strtotime($hizmet->bitis_tarihi) - time();
        $bitis_tarihi = floor($bitis_tarihi / (60*60*24) );
    }
?>
@extends('layouts.danisan-admin-layout')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
        <a href="#">Hoşgeldiniz {{ $danisan->ad }} {{ $danisan->soyad }}</a>
        </li>
        <li class="breadcrumb-item active">Panel</li>
      </ol>
	  <!-- Icon Cards-->
      <div class="row">
        <div class="col-xl-4 col-sm-6 mb-3">
          <div class="card dashboard text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-user-md"></i>
              </div>
                @if( isset($danisan->diyetisyen->id) )
                <div class="mr-5"><h5>Diyetisyeniniz:<br> {{ $danisan->diyetisyen->ad }} {{ $danisan->diyetisyen->soyad }}</h5></div>
                @else
                <div class="mr-5"><h5>Diyetisyeniniz bulunmamaktadır.</h5></div>
                @endif
            </div>
                @if( isset($danisan->diyetisyen->id) )
                <a class="card-footer text-white clearfix small z-1" href="{{ route('messages.show', $danisan->diyetisyen->id) }}">
                  <span class="float-left">Mesaj Gönderin</span>
                  <span class="float-right">
                    <i class="fa fa-angle-right"></i>
                  </span>
                </a>
                @else
                <a class="card-footer text-white clearfix small z-1" href="{{ URL('diyetisyens') }}">
                  <span class="float-left">Diyetisyenlere Göz Atın</span>
                  <span class="float-right">
                    <i class="fa fa-angle-right"></i>
                  </span>
                </a>
                @endif
          </div>
        </div>
        <div class="col-xl-4 col-sm-6 mb-3">
          <div class="card dashboard text-white bg-warning o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-check"></i>
              </div>
              @if(isset($vki))
              <div class="mr-5"><h5>Vücut Kitle İndeksiniz:<br>
              {{ number_format($vki, 2) }}
              @else
              <div class="mr-5"><h5>Vücut Kitle İndeksinizi Hesaplamak İçin Ölçülerinizi Giriniz.<br>
              @endif
            </h5>
            </div>
            </div>
            <span class="card-footer text-white clearfix small z-1">
              <span class="float-left">
                @if(isset($vki))
                  @if($vki <= 18.8)
                  Zayıf
                  @elseif($vki > 18.8 && $vki < 25)
                  Normal
                  @elseif($vki > 25 && $vki < 30)
                  Hafif Kilolu
                  @elseif($vki >= 30)
                  Obez
                  @endif
                @else
                  <a style="color: #fff !important" href="{{ URL('danisan-olculeri') }}">Ölçülerime Git</a>
                @endif
              </span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </span>
          </div>
        </div>
        @if( isset($danisan->diyetisyen->id) )
        <div class="col-xl-4 col-sm-6 mb-3">
          <div class="card dashboard text-white bg-success o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-calendar-times-o"></i>
              </div>
              <div class="mr-5"><h5>Hizmet Sürenizin Bitmesine <b>{{$bitis_tarihi}}</b> Gün Kaldı.</h5></div>
            </div>
          </div>
        </div>
        @endif
		</div>
		<!-- /cards -->
		<h2></h2>
      <div class="box_general padding_bottom">
        <div class="header_box version_2">
          <h2><i class="fa fa-bar-chart"></i>Kilo Değişimi</h2>
        </div>
      <canvas id="myAreaChart" width="100%" height="30" style="margin:45px 0 15px 0;"></canvas>
      </div>
	   </div>
	  <!-- /.container-fluid-->
    </div>
       
@endsection