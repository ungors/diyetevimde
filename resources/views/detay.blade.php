<?php 
	
use App\Danisan;
use App\Odeme;

session_start();
ob_start();

	if(isset($_SESSION["danisan"])) 
	{		
	    $user = Danisan::where("email", $_SESSION["danisan"])->first();

			if(!empty($user->diyetisyen_id)) 
			{
				if($user->diyetisyen_id == $detay->id) 
				{
					$kontrol = 1;
					$hizmet = Odeme::where("danisan_id", $user->id)->latest("id")->first();
					
					$bitis_tarihi = strtotime($hizmet->bitis_tarihi) - time();
					$bitis_tarihi = floor($bitis_tarihi / (60*60*24) );
				} 
				else 
				{
					$kontrol = 5;
				}
			} 
			else 
			{
				$kontrol = 2;
			}
	}
	elseif(isset($_SESSION["diyetisyen"])) 
	{
		$kontrol = 3;
	}
	else 
	{
		$kontrol = 4;
	}

?>

@extends('layouts.layout')

@section('title')
@if( $detay->unvan )
	{{$detay->unvan}}.
@endif {{$detay->ad}} {{$detay->soyad}}
@endsection

@section('content')
<main>
		<div id="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="/index">Anasayfa</a></li>
					<li><a href="/diyetisyens">Diyetisyenler</a></li>
					<li>{{$detay->ad}} {{$detay->soyad}}</li>
				</ul>
			</div>
		</div>
		<!-- /breadcrumb -->

		<div class="container margin_60">
			<div class="row">
				<div class="col-xl-8 col-lg-8" id="diyetisyen">
					<nav id="secondary_nav">
						<div class="container">
							<ul class="clearfix">
								<li><a href="#section_1" class="active">Genel Bilgiler</a></li>
								<li><a href="#section_2">Yorumlar</a></li>
							</ul>
						</div>
					</nav>
					<div id="section_1">
						<div class="box_general_3">
							<div class="profile">
								<div class="row">
									<div class="col-lg-3 col-md-4">
										<figure>
											<img src="/uploads/{{$detay->p_foto}}" alt="" class="img-fluid" >
										</figure>
									</div>
									<div class="col-lg-9 col-md-8">
									<h1>
										@if( $detay->unvan )
											{{$detay->unvan}}.
										@endif
										{{$detay->ad}} {{$detay->soyad}}</h1>
									<span class="rating">
										@for($i = 0; $i < 5; $i++)
											@if($i < ceil($detay->puan))
												<i class="icon_star voted"></i>
											@else
												<i class="icon_star"></i>
											@endif
										@endfor
										<small>{{$detay->puan}}</small></span>
										<ul class="statistic">
											{{-- <li>854 Yorum</li> --}}
											<li>{{$detay->danisan_sayisi}} kez hizmet verdi.</li>
										</ul>
										<ul class="contacts">
											<li>
												<h6 style="display: inline-block; margin-right: 5px">Kişisel Telefonu:</h6> {{$detay->telefon}}</li>
												<li>
													<h6 style="display: inline-block; margin-right: 5px">Ofis Telefonu:</h6> {{$detay->ofis_telefon}}</li>
										</ul>
									</div>
								</div>
							</div>
							
							<hr>
							
							<!-- /profile -->
							<div class="indent_title_in">
								<i class="pe-7s-user"></i>
								<h3>Uzman Bilgileri</h3>
								<p>{{$detay->unvan}}. {{$detay->ad}} {{$detay->soyad}} Hakkındaki Bilgiler</p>
							</div>
							<div class="wrapper_indent">
								<p>{{$detay->bio}}</p>
								<h6>Hizmet Alanları</h6>
								<div class="row">
									<div class="col-lg-12">
										<ul class="bullets">
											@foreach(explode(',',$detay->hizmetler) as $row)
												<li>{{ $row }}</li>
											@endforeach
										</ul>
									</div>
								</div>
								<!-- /row-->
							</div>
							<!-- /wrapper indent -->

							<hr>

							<div class="indent_title_in">
								<i class="pe-7s-news-paper"></i>
								<h3>Eğitim Bilgileri</h3>
								<p>@if( $detay->unvan )
										{{$detay->unvan}}.
									@endif {{$detay->ad}} {{$detay->soyad}} Eğitim Bilgileri</p>
							</div>
							<div class="wrapper_indent">								<h6></h6>
								<ul class="list_edu">
									@foreach(preg_split('/\r\n|\r|\n/', $detay->egitim) as $row)
										<li>{{ $row }}</li>
									@endforeach
								</ul>
							</div>
							<!--  End wrapper indent -->

							<hr>

							<div class="indent_title_in">
								<i class="pe-7s-cash"></i>
								<h3>Ücretlendirme</h3>
								<p>@if( $detay->unvan )
										{{$detay->unvan}}.
									@endif {{$detay->ad}} {{$detay->soyad}} hizmet ücreti ile ilgili detaylar</p>
							</div>
							<div class="wrapper_indent">
								<table class="table table-responsive table-striped">
									<thead>
										<tr>
											<th>Süre</th>
											<th>Ücret</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1 Ay</td>
											<td><span class="d-ucret">{{$detay->ucret}}</span> ₺</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!--  /wrapper_indent -->
						</div>
						<!-- /section_1 -->
					</div>
					<!-- /box_general -->
					<!-- /section_2 -->
				</div>
				<!-- /col -->
				<aside class="col-xl-4 col-lg-4" id="sidebar">
					<div class="box_general_3 booking">
						@if($kontrol == 2)
							<div class="box-inner">
								<div class="row">
									<div class="col-12">
										
											@include('common.errors')

										{!! Form::open(['action' => 'OdemeController@store' , 'method' => 'POST']) !!}
										<div class="form-group">
												<select class="form-control price-select" name="ay">
													<option selected value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
													<option value="12">12</option>
												</select>
											</div>
											<label class="price">Hizmet Bedeliniz: <span>{{$detay->ucret * 1}} ₺</span></label>
											<div class="odeme-form">
												<div class="form-group">
													<input type="text" name="Name" class="form-control mb-15" placeholder="Kart Üstündeki Ad Soyad">
													<input type="text" name="Card-Number" class="form-control mb-15" placeholder="Kart Numarası">
													<input type="text" name="M/Y" class="form-control mb-15" placeholder="M/Y">
													<input type="text" name="CVV" class="form-control mb-15" placeholder="CVV">
													<input type="hidden" name="diyetisyen_id" value="{{$detay->id}}">
													<input type="hidden" name="danisan_id" value="{{$user->id}}">
													<input id="ucret" name="ucret" type="hidden" value="{{$detay->ucret * 1}}">
													<input name="tarih" type="hidden" value="{{ date('Y-m-d') }}">
												</div>
											</div>
									</div>
								</div>
								<hr>
								<button class="btn_1 full-width">Hizmet Almaya Başla</button>
								{!! Form::close() !!}
							</div>
						@elseif($kontrol == 1)
							Hizmet sürenizin bitmesine <b>{{ $bitis_tarihi }}</b> gün kaldı.
						@elseif($kontrol == 3)
							@section("script")
								<script>
									$("#sidebar").remove();
									$("#diyetisyen").removeClass().addClass("col-md-12");
								</script>
							@endsection

						@elseif($kontrol == 5)
							<b>Zaten bir diyetisyenden hizmet alıyorsunuz.</b>
						@elseif($kontrol == 4)
							<a href="/danisan-giris" class="btn_1 full-width" style="padding-left: 10px; padding-right: 10px">Başlamak İçin Giriş Yapınız.</a>
						@endif
						
					</div>
					<!-- /box_general -->
				</aside>
				<!-- /asdide -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</main>
	<!-- /main -->
	@endsection

