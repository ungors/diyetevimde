@extends('layouts.layout')

@section('title')
Bizimle İletişime Geçin
@endsection

@section('content')
<main>
		<div class="container margin_60_35">
			<div class="row">
				<aside class="col-lg-3 col-md-3">
					<div id="contact_info">
						<h3>İletişim Bilgileri</h3>
						<p>
							Ataşehir/İstanbul<br> 0(212) 569 3400<br>
							<a href="#">info@diyetevimde.com</a>
						</p>
					</div>
				</aside>
				<!--/aside -->
				<div class=" col-lg-9 col-md-9 ml-auto">
					<div class="box_general">
						<h3>Bizimle iletişime geçin</h3>
						<div>
							<div id="message-contact"></div>

							

								@include('common.errors')

								{!! Form::open(['action' => 'HomeController@mesajGonder' , 'method' => 'POST']) !!}
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<div class="form-group">
											{{Form::text('Ad','',['class' => 'form-control' , 'placeholder' => 'Adınız'])}}
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="form-group">
											{{Form::text('Soyad','',['class' => 'form-control' , 'placeholder' => 'Soyadınız'])}}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<div class="form-group">
											{{Form::text('E-mail','',['class' => 'form-control' , 'placeholder' => 'E-posta adresiniz'])}}

										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="form-group">
											{{Form::text('Telefon','',['class' => 'form-control' , 'placeholder' => 'Telefon numaranız'])}}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											{{ Form::textarea('Mesaj',null,['id' => 'article-ckeditor','class'=>'form-control', 'style'=> 'height:100px;' ,'rows' => 5 , 'placeholder' => 'Mesajınız']) }}
										</div>
									</div>
								</div>
							
								{{Form::submit('Gönder',['class' => 'btn_1'])}}
							{!! Form::close() !!}
						</div>
						<!-- /col -->
					</div>
				</div>
				<!-- /col -->
			</div>
			<!-- End row -->
		</div>
		<!-- /container -->
</main>
	<!-- /main -->
@endsection
   