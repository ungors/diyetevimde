{{-- @include('header') --}}

@extends('layouts.layout')

@section('title')
Danışan Kayıt Sayfası
@endsection

@section('content')

<main>
	<div class="bg_color_2">
		<div class="container margin_60_35">
			<div id="register">
				<div class="row justify-content-center">
					<div class="col-md-5">
						
						@include('common.errors')
								
						{!! Form::open(['action' => 'DanisanController@store' , 'method' => 'POST']) !!}
							<div class="box_form">
								<div class="form-group">
									{{Form::text('Name','',['class' => 'form-control' , 'placeholder' => 'Adınız'])}}
								</div>
								<div class="form-group">										{{-- <input type="text" class="form-control" placeholder="Your last name"> --}}
									{{Form::text('LastName','',['class' => 'form-control' , 'placeholder' => 'Soyadınız'])}}
								</div>
								<div class="form-group">										{{-- <input type="email" class="form-control" placeholder="Your email address"> --}}
									{{Form::text('Email','',['class' => 'form-control' , 'placeholder' => 'E-posta adresiniz'])}}

								</div>
								<div class="form-group">										{{-- <input type="password" class="form-control" id="password1" placeholder="Your password"> --}}
									{{Form::password('Şifre', ['class' => 'form-control' , 'placeholder' => 'Şifreniz']) }}
								</div>
								<div class="form-group">										{{-- <input type="password" class="form-control" id="password2" placeholder="Confirm password"> --}}
									{{Form::password('ŞifreTekrar', ['class' => 'form-control' , 'placeholder' => 'Şifrenizin Tekrarı']) }}
								</div>
								<div class="checkbox-holder text-left">
									<div class="checkbox_2">
										{{ Form::checkbox('check_2', 'accept_2', true) }}
										<label for="check_2"><span><strong>Şartları &amp; Sözleşmeleri</strong> okudum ve kabul ediyorum.</span></label>
									</div>
								</div>
								<div class="form-group text-center add_top_30">
									{{Form::submit('Kayıt Ol',['class' => 'btn_1 btn_register'])}}
								</div>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /register -->
		</div>
	</div>
</main>
	<!-- /main -->
@endsection
  