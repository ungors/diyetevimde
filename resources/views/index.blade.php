<?php  use App\Diyetisyen;  ?>

@extends('layouts.layout')

@section('title')
diyetevimde.com | Sana en uygun diyetisyeni kolayca bul ve diyete başla.
@endsection

@section('content')
	<main>
		<div class="hero_home version_2">
			<div class="content">
				<h3>Diyetisyen Ara!</h3>
				<p>
					Sana en uygun diyetisyeni bul ve kilo vermeye hazır ol.
				</p>
				
				@include('common.errors')

				{!! Form::open(['action' => 'DiyetisyenController@ara' , 'method' => 'POST']) !!}
					<div id="custom-search-input">	
					<div class="input-group">
						{{Form::text('Arama',null,['class' => 'search-query' , 'placeholder' => 'Diyetisyen (Başak Öztürk)...'])}}
					
						{{Form::submit('Ara',['class' => 'btn_search'])}}
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /Hero -->

		<div class="container margin_120_95">
			<div class="main_title">
				<h2>Online <strong>diyete</strong> hemen başla!</h2>
				<p>Uzman diyetisyenler içerisinden sana en uygun olanı seç ve kilo vermeye başla. </p>
			</div>
			<div class="row add_bottom_30">
				<div class="col-lg-4">
					<div class="box_feat" id="icon_1">
						<span></span>
						<img src="{{URL::asset('img/worker.png')}}">
						<h3>Diyetisyen Ara</h3>
						<p>Yüzlerce diyetisyen arasından en uygununu ara.</p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="box_feat" id="icon_2">
						<span></span>
						<img src="{{URL::asset('img/doctor.png')}}">
						<h3>Profili Görüntüle</h3>
						<p>Seçtiğin diyetisyen hakkında detaylı bilgiye ulaş.</p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="box_feat" id="icon_3">
						<img src="{{URL::asset('img/checked.png')}}">
						<h3>Diyete başla</h3>
						<p>Diyetisyenle görüş kolayca ödemeni yap ve diyete başla.</p>
					</div>
				</div>
			</div>
			<!-- /row -->
		<p class="text-center"><a href="{{URL('/diyetisyens')}}" class="btn_1 medium">Diyetisyen Bul!</a></p>

		</div>
		<!-- /container -->

		<div class="bg_color_1">
			<div class="container margin_120_95">
				<div class="main_title">
					<h2>En Çok Tercih Edilen Diyetisyenler</h2>
				</div>
				<?php  $diyetisyenler = Diyetisyen::where('onay',1)->get(); ?>
				<div id="reccomended" class="owl-carousel owl-theme">
					
						@foreach ($diyetisyenler as $item)
						<div class="item">
							<a href="{{ route('diyetisyens.show', $item->slug) }}">
								<div class="title">
									<h4>{{$item->ad}} {{$item->soyad}}<em>Diyetisyen</em></h4>
								</div><img src="uploads/{{$item->p_foto}}" height="310" alt="">
							</a>
						</div>
						@endforeach		
					
				</div>
				<!-- /carousel -->
			</div>
			<!-- /container -->
		</div>
		<!-- /white_bg -->
		<!-- /container -->
</main>

@endsection
