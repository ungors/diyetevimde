@extends('layouts.admin-layout')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/admin-index">Panel</a>
        </li>
        <li class="breadcrumb-item active">Blog Düzenle</li>
      </ol>
      <div class="box_general padding_bottom">
        <div class="header_box version_2">
          <h2><i class="fa fa-file"></i>Blog Düzenle</h2>
        </div>

        @include('common.errors')
        
        {!! Form::open(['action' => ['BlogController@update', $blog->id ], 'method' => 'POST' , 'files'=>'true']) !!}
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Blog Başlığı</label>
              {{Form::text('Baslik',$blog->baslik ,['class' => 'form-control' , 'placeholder' => 'Blog Başlığı'])}}
            </div>
          </div>
        </div>
        <!-- /row-->
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Blog İçeriği</label>
              {{ Form::textarea('İcerik',$blog->icerik,['id' => 'article-ckeditor', 'class'=>'form-control', 'rows' => 5 , 'placeholder' => 'Blog İçeriği Giriniz...']) }}
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label style="display: block">Blog Resmi</label>
              <img width="100px" src="/uploads/{{$blog->resim}}">
              <br>
              {{ Form::file('Resim',null)}}
            </div>
          </div>
        </div>
      </div>
      {{Form::hidden('_method','PUT')}}
      <p>  {{Form::submit('Düzenle',['class' => 'btn_1 medium'])}} </p>
      {!! Form::close() !!}

    </div>
    <!-- /.container-fluid-->
  </div>
  <!-- /.container-wrapper-->

@endsection