<?php
  use App\Danisan;
  use App\Odeme;
?>
@extends('layouts.admin-layout')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
        <a href="#">Hoşgeldiniz {{$user->ad}} {{$user->soyad}}</a>
        </li>
        <li class="breadcrumb-item active">Panel</li>
      </ol>
	  <!-- Icon Cards-->
      <div class="row">
        <div class="col-xl-4 col-sm-6 mb-3">
          <div class="card dashboard text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-try"></i>
              </div>
            <div class="mr-5"><h5>Hesabım: {{ Odeme::where("diyetisyen_id", $user->id)->sum("ucret") }} TL</h5></div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-sm-6 mb-3">
          <div class="card dashboard text-white bg-warning o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-list"></i>
              </div>
				      <div class="mr-5"><h5>{{ count(DB::table('blogs')->where('diyetisyen_id',$user->id)->get()) }} Adet Blogunuz Var</h5></div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href=" {{ URL("admin-blogs") }} ">
              <span class="float-left">Bloglarıma Git</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-4 col-sm-6 mb-3">
          <div class="card dashboard text-white bg-success o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-users"></i>
              </div>
              <div class="mr-5"><h5>{{ count(Danisan::where('diyetisyen_id',$user->id)->get()) }} Danışanınız Bulunmaktadır.</h5></div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href=" {{ URL("admin-bookings") }} ">
              <span class="float-left">Danışanlarıma Git</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
		</div>
		<!-- /cards -->
		<h2></h2>
		<div class="box_general padding_bottom">
			<div class="header_box version_2">
				<h2><i class="fa fa-bar-chart"></i>Hesabım</h2>
			</div>
		 <canvas id="myAreaChart" width="100%" height="30" style="margin:45px 0 15px 0;"></canvas>
		</div>
	  </div>
	  <!-- /.container-fluid-->
       </div>
       
@endsection