<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Diyetisyen extends Model 
{
    protected $table   = 'diyetisyens';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function blogs()
    {
        return $this->hasMany('App\Blog','diyetisyen_id','id');
    }

    public function yorumlar()
    {
        return $this->hasMany('App\DiyetisyenYorum','diyetisyen_id','id');
    }

    public function danisanlar()
    {
        return $this->hasMany('App\Danisan','diyetisyen_id','id');
    }

}


