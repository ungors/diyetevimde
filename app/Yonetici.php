<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Yonetici extends Model
{
    protected $table   = 'yoneticis';
    public $primaryKey = 'id';
    public $timestamps = true;
}
