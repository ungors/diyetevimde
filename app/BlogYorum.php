<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogYorum extends Model
{
    protected $table   = 'blog_yorums';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function blog()
    {
        return $this->belongsTo('App\Blog');
    }

    public function danisan()
    {
        return $this->belongsTo('App\Danisan');
    }
}
