<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiyetisyenYorum extends Model
{
    protected $table   = 'diyetisyen_yorums';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function diyetisyen()
    {
        return $this->belongsTo('App\Diyetisyen');
    }

    public function danisan()
    {
        return $this->belongsTo('App\Danisan');
    }
}
