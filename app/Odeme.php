<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Odeme extends Model
{
    protected $table   = 'odemes';
    public $primaryKey = 'id';
    public $timestamps = true;
}
