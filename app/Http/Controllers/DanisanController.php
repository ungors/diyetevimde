<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Diyetisyen;
use App\VucutOlcusu;
use App\Danisan;
use App\Odeme;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;
use Mail;

class DanisanController extends Controller
{
   
    public function index()
    {
        //
    }

    public function danisanProfile()
    {
        ob_start();
        session_start();

        return view('danisan-profile')->with('danisan',Danisan::where('email',$_SESSION['danisan'])->first());
    }

    public function create()
    {
        return View('danisan-kayit');
    }

    public function store(Request $request)
    {

        $rules = [
            'Name'        => 'required|alpha',
            'LastName'    => 'required|alpha',
            'Email'       => 'required|email|unique:danisans,email',
            'Şifre'       => 'required|min:8',
            'ŞifreTekrar' => 'required_with:Şifre|same:Şifre'
        ];

        $customMessages = [
            'required' => 'Lütfen :attribute alanını giriniz!',
            'min' => ':attribute alanı en az :min karakter içermelidir!',
            'email' => 'Geçersiz :attribute!',
            'unique' => 'Girdiğiniz :attribute sistemde kayıtlı!',
            'alpha' => ':attribute alanı sadece harflerden oluşmalı!',
            'same' => ':attribute, şifre ile aynı olmalı!',
            'required_with' => ':attribute alanı :values varken zorunludur!'
        ];

        $this->validate($request,$rules,$customMessages);

        $danisan = new Danisan;
        $danisan->ad    = $request->input('Name');
        $danisan->soyad = $request->input('LastName');
        $danisan->email = $request->input('Email');
        $danisan->sifre = $request->input('Şifre');
        $danisan->sifre_tekrar = $request->input('ŞifreTekrar');

        $danisan->save();

        // Mail::send('emails.hello', array('nick' => $danisan->ad), function($message)
        // {
        //     $message->to($danisan->email, $danisan->ad)->subject('Hoşgeldiniz!');
        // });   

        return Redirect('/danisan-giris');
    }

    public function show($id)
    {
        //
    }

    public function edit()
    {
        ob_start();
        session_start();
        if(isset($_SESSION['danisan']))
        {
            $email = $_SESSION['danisan'];
            $danisan = DB::table('danisans')->where('email',$email)->first();
            $id = $danisan->id;
            return view('danisan-edit')->with('danisan',$danisan)
            ->with('danisan->id',$id);
        }
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'Ad'        => 'required|alpha',
            'Soyad'    => 'required|alpha',
            'Email'       => 'required|email',
            'Boy'       => 'nullable|integer|max:300',
            'Profil' => 'image',
            'E_Sifre'  => 'nullable|exists:danisans,sifre',
            'Y_Sifre'       => 'nullable|required_with:E_Sifre|min:8',
            'TY_Sifre' => 'required_with:Y_Sifre|same:Y_Sifre'
        ];

        $customMessages = [
            'required' => 'Lütfen :attribute alanını giriniz!',
            'integer' => ':attribute alanı sayısal değer olmalıdır!',
            'min' => ':attribute alanı en az :min karakter içermelidir!',
            'email' => 'Geçersiz :attribute!',
            'Boy.max' => ':attribute en fazla :max olabilir!',
            'exists' => 'Lütfen geçerli :attribute giriniz!',
            'alpha' => ':attribute alanı sadece harflerden oluşmalı!',
            'same' => ':attribute, şifre ile aynı olmalı!',
            'required_with' => ':attribute alanı :values varken zorunludur!'
        ];

        $this->validate($request,$rules,$customMessages);

        ob_start();
        session_start();

        $file = $request->file('Profil');

        $danisan = Danisan::find($id);

        $danisan->ad = $request->input('Ad');
        $danisan->soyad = $request->input('Soyad'); 
        $danisan->email = $request->input('Email');
        $danisan->boy = $request->input('Boy');

        if(!empty($request->input('E_Sifre')) && !empty($request->input('Y_Sifre')))
        {
            $danisan->sifre = $request->input('Y_Sifre');
            $danisan->sifre_tekrar = $request->input('TY_Sifre');
        }
        
        if(!empty($file)) 
        {
            $danisan->p_foto = $file->getClientOriginalName();
            $resim_hedef = 'uploads';
            $file->move($resim_hedef,$file->getClientOriginalName());
        }

        $danisan->save();
        return redirect('/danisan-edit');
    }

    public function danisanMessages()
    {
        ob_start();
        session_start();
        if(isset($_SESSION['danisan']))
        {
            $email = $_SESSION['danisan'];
            $user = DB::table('danisans')->where('email',$email)->first();
            return view('danisan-messages')->with('diyetisyens',Diyetisyen::where('id',$user->diyetisyen_id)->get());
        }
    }

    public function destroy($id)
    {
        //
    }

    public function olcuduzenle(Request $request)
    {

        $rules = [
            'Kilo'  => 'required|numeric|min:10|max:500',
        ];

        $customMessages = [
            'required' => 'Lütfen :attribute alanını giriniz!',
            'numeric' => ':attribute alanı sayısal değer olmalıdır!',
            'Kilo.min' => ':attribute alanı en az :min olabilir!',
            'Kilo.max' => ':attribute en fazla :max olabilir!'
        ];

        $this->validate($request,$rules,$customMessages);

        ob_start();
        session_start();

        if(isset($_SESSION['danisan']))
        {
            $email = $_SESSION['danisan'];
            $user = DB::table("danisans")->where('email',$email)->first();
            $olcu = new VucutOlcusu;

            $olcu->kilo = $request->input('Kilo');
            $olcu->danisan_id = $user->id;

            $olcu->save();

            return redirect('/danisan-olculeri');
        }
    }

    public function danisan_login(Request $request)
    {
        $rules = [
            'Email'  => 'required|email|exists:danisans,email',
            'Şifre'  => 'required|exists:danisans,sifre',
        ];

        $customMessages = [
            'required' => 'Lütfen :attribute giriniz!',
            'email' => 'Geçersiz :attribute!',
            'exists' => 'Girdiğiniz :attribute yanlış!'
        ];

        $this->validate($request,$rules,$customMessages);

        $email = $request->input('Email');
        $sifre = $request->input('Şifre');

        $kullanici = Danisan::where([
            ["email", "=", $email],
            ["sifre", "=", $sifre]
        ])->first();
        

        if($kullanici)
        {   
            ob_start();
            session_start();

            $_SESSION['danisan'] = $email;

            $odeme_detay = Odeme::where("danisan_id", 1)->latest("id")->first();

            $bitis_tarihi = $odeme_detay->bitis_tarihi;

            if(time() >= strtotime($bitis_tarihi)) {
                Danisan::where([
                    ["email", "=", $email]
                ])->update([
                    "diyetisyen_id" => null
                ]);
            }

            return redirect('index');
        }
        else
        {
            return redirect('danisan-giris');
        }
    }

    public function danisan_logout()
    {
        session_start();
        session_destroy();
        return redirect('danisan-giris');
    }
}
