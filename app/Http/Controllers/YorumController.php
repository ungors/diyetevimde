<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlogYorum;
use App\Danisan;
use App\Blog;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class YorumController extends Controller
{
  
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rules = [
            'Yorum' => 'required'
        ];

        $customMessages = [
            'required' => 'Lütfen :attribute giriniz!',
            
        ];

        $this->validate($request,$rules,$customMessages);

        ob_start();
        session_start();

        if(isset($_SESSION['danisan']))
        {
            $email = $_SESSION['danisan'];
            $user = DB::table('danisans')->where('email',$email)->first();
            
            $yorum = new BlogYorum;
            $yorum->icerik     = $request->input('Yorum');
            $yorum->danisan_id = $user->id;
            $yorum->blog_id    = $request->input('blog_id');

            $yorum->save();

            return View('blog-detay',['detay' => Blog::findOrFail($yorum->blog_id)]);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
