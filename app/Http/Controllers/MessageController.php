<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Message;

class MessageController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        $message = new Message;
        $message->sender_id = $request->input('sender_id');
        $message->receiver_id = $request->input('receiver_id');
        $message->content = $request->input('Content');
        $file = $request->file('message-file');

        if(!empty($file)) 
        {
            $message->file = $file->getClientOriginalName();
            $resim_hedef = 'uploads';
            $file->move($resim_hedef,$file->getClientOriginalName());
        }

        $message->save();

        return Redirect(url()->previous());
    }

    public function show($id)
    {
        session_start();
        ob_start();

        if(isset($_SESSION['diyetisyen']))
        {
            $danisan_id = $id;
            $email = $_SESSION['diyetisyen'];
            $user = DB::table('diyetisyens')->where('email',$email)->first();
            $receiver_user = DB::table('danisans')->where('id',$danisan_id)->first();
            $messages = DB::table('messages')
            ->where([
                ['sender_id', '=', $user->id],
                ['receiver_id', '=', $danisan_id],
            ])
            ->orWhere([
                ['receiver_id', '=', $user->id],
                ['sender_id', '=', $danisan_id],
            ])
            ->get();

            return view('admin-message-detail')->with('messages',$messages)->with('receiver_user',$receiver_user)->with('user',$user);
        }

        elseif(isset($_SESSION['danisan']))
        {
            $diyetisyen_id = $id;
            $email = $_SESSION['danisan'];
            $user = DB::table('danisans')->where('email',$email)->first();
            $receiver_user = DB::table('diyetisyens')->where('id',$diyetisyen_id)->first();
            $messages = DB::table('messages')
            ->where([
                ['sender_id', '=', $user->id],
                ['receiver_id', '=', $diyetisyen_id],
            ])
            ->orWhere([
                ['receiver_id', '=', $user->id],
                ['sender_id', '=', $diyetisyen_id],
            ])
            ->get();

            return view('danisan-message-detail')->with('messages',$messages)->with('receiver_user',$receiver_user)->with('user',$user);
        }
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
