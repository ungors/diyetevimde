<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\MessageBag;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\ValidationServiceProvider;
use App\Blog;
use App\Diyetisyen;
use App\Danisan;


class BlogController extends Controller
{

    public function index()
    {
        $bloglar =  Blog::orderBy('created_at', 'asc')->take(4)->get();

        return View('/blog')->with('blogs',Blog::paginate(5))->with('recents', $bloglar);
    }

    public function adminBlogs()
    {
        ob_start();
        session_start();
        if(isset($_SESSION['diyetisyen']))
        {
            $email = $_SESSION['diyetisyen'];
            $user = DB::table('diyetisyens')->where('email', $email)->first();
            $blogs = Diyetisyen::find($user->id)->blogs()->paginate(1);
            return View('admin-blogs')->with('bloglar',$blogs);
        }
    }

    public function create()
    {
        ob_start();
        session_start();
        if(isset($_SESSION['diyetisyen']))
        {
            return View('/blog-olusturma');
        }
        else
        {
            return redirect('/giris');
        }
    }

    public function store(Request $request)
    {

        $rules = [
            'Baslik' => 'required|max:300',
            'İcerik' => 'required',
            'Resim'  => 'required|image'
        ];

        $customMessages = [
            'required' => 'Lütfen :attribute giriniz!',
            'image' => ':attribute alanı resim dosyası olmalıdır!',
            'max' => ':attribute alanı en fazla :max karakter içermelidir!'
        ];

        $this->validate($request,$rules,$customMessages);

        ob_start();
        session_start();

        if(isset($_SESSION['diyetisyen']))
        {
            $email = $_SESSION['diyetisyen'];
            $file = $request->file('Resim');
            $user = DB::table('diyetisyens')->where('email', $email)->first();
            $blog = new Blog;
            $blog->baslik           = $request->input('Baslik');
            $blog->icerik           = $request->input('İcerik');
            $blog->resim            = $file->getClientOriginalName();
            $resim_hedef = 'uploads';
            $file->move($resim_hedef,$file->getClientOriginalName());

            $blog->diyetisyen_id    = $user->id;
            $blog->slug = str_slug($blog->baslik);

            $blog->save();

            return redirect('/admin-index');
        }

        else
        {
            return redirect('/giris');      
        }
    }

    public function show($slug)
    {
        ob_start();
        session_start();

        $blog = Blog::where('slug',$slug)->first();
        $count = $blog->visit_count;
        $count++;
        Blog::where([
            ["slug", "=" ,$slug]
        ])->update([
            "visit_count" => $count
        ]);

        $bloglar =  Blog::orderBy('created_at', 'asc')->take(4)->get();

        return View('blog-detay',['detay' => Blog::where('slug',$slug)->first()])->with('recents', $bloglar);
    }

    public function edit($slug)
    {
        return View('blog-edit',['blog' => Blog::where('slug',$slug)->first()]);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'Baslik' => 'required|max:300',
            'İcerik' => 'required',
            'Resim'  => 'image'
        ];

        $customMessages = [
            'required' => 'Lütfen :attribute giriniz!',
            'image' => ':attribute alanı resim dosyası olmalıdır!',
            'max' => ':attribute alanı en fazla :max karakter içermelidir!'
        ];

        $this->validate($request,$rules,$customMessages);

        ob_start();
        session_start();

        if(isset($_SESSION['diyetisyen']))
        {
            $email = $_SESSION['diyetisyen'];
            $file = $request->file('Resim');
            $user = DB::table('diyetisyens')->where('email', $email)->first();
            $blog = Blog::find($id);
            $blog->baslik           = $request->input('Baslik');
            $blog->icerik           = $request->input('İcerik');
        
            if(!empty($file)) 
            {
                $blog->resim = $file->getClientOriginalName();
                $resim_hedef = 'uploads';
                $file->move($resim_hedef,$file->getClientOriginalName());
            }

            $blog->diyetisyen_id    = $user->id;
            $blog->slug = str_slug($blog->baslik);

            $blog->save();

            return redirect('/admin-index');
        }

        else
        {
            return redirect('/giris');      
        }
    }

    public function destroy($id)
    {
        $blog = Blog::find($id);
        $blog->delete();
        return redirect('/admin-blogs');
    }

    public function ara(Request $request)
    {

        $aranan = $request->input('Arama');
        
        $sonuclar = Blog::where('baslik','like', '%'. $aranan . '%')->get();

        $bloglar =  Blog::orderBy('created_at', 'asc')->take(4)->get();

        
        
        if($aranan != "") {
            return View('/blog')->with('blogs',$sonuclar)->with('recents', $bloglar)->with('search', TRUE)->with("arama", $aranan);
        } else {
            return View('/blog')->with('blogs', Blog::all())->with('recents', $bloglar)->with('search', FALSE)->with("arama", "");
        }

    }
}
