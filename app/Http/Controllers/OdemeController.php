<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Odeme;
use App\Danisan;
use App\Diyetisyen;

class OdemeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ödeme');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'Name'        => 'required|alpha',
            'Card-Number'    => 'required',
            'M/Y'       => 'required',
            'CVV'       => 'required',
        ];

        $customMessages = [
            'required' => 'Lütfen :attribute alanını giriniz!'
        ];

        $this->validate($request,$rules,$customMessages);
        $odeme = new Odeme;

        $odeme->diyetisyen_id = $request->input('diyetisyen_id');
        $odeme->danisan_id = $request->input('danisan_id');
        $odeme->ucret = $request->input('ucret');
        $odeme->ay = $request->input('ay');
        $odeme->bitis_tarihi = date("Y-m-d", strtotime("+ $odeme->ay months", strtotime($request->input('tarih')))); 
        $odeme->save();

        Danisan::where("id", $odeme->danisan_id)
        ->update([ "diyetisyen_id" => $odeme->diyetisyen_id]);

        $user = Diyetisyen::where('id',$odeme->diyetisyen_id)->first();
        $d_sayisi = $user->danisan_sayisi;
        $d_sayisi++;
        Diyetisyen::where("id", $odeme->diyetisyen_id)
        ->update([ "danisan_sayisi" => $d_sayisi ]);
        

        return redirect()->back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
