<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Diyetisyen;
use App\Danisan;
use App\Message;
use Session;

class DiyetisyenController extends Controller
{
   
    public function index()
    { 
        ob_start();
        session_start();

        return View('diyetisyenler')->with('lists',Diyetisyen::where("onay", 1)->paginate(5));
    }

    public function adminIndex()
    {
        ob_start();
        session_start();

        if(isset($_SESSION['diyetisyen']))
        {
            $user = DB::table('diyetisyens')->where('email', $_SESSION['diyetisyen'])->first();
            return View('admin-index')->with('user', $user);
        }

        else
        {
            return redirect('/giris');
        }
    }

    public function adminMessages()
    {
        ob_start();
        session_start();

        if(isset($_SESSION['diyetisyen']))
        {
            $email = $_SESSION['diyetisyen'];
            $user = DB::table('diyetisyens')->where('email',$email)->first();
            return view('admin-messages');
        }
    }

    public function create()
    {
        return View('diyetisyen-kayit');
    }

    public function store(Request $request)
    {
        $rules = [
            'Name'        => 'required|alpha',
            'LastName'    => 'required|alpha',
            'Email'       => 'required|email|unique:diyetisyens,email',
            'Şifre'       => 'required|min:8',
            'ŞifreTekrar' => 'required_with:Şifre|same:Şifre'
        ];

        $customMessages = [
            'required' => 'Lütfen :attribute alanını giriniz!',
            'min' => ':attribute alanı en az :min karakter içermelidir!',
            'email' => 'Geçersiz :attribute!',
            'unique' => 'Girdiğiniz :attribute sistemde kayıtlı!',
            'alpha' => ':attribute alanı sadece harflerden oluşmalı!',
            'same' => ':attribute, şifre ile aynı olmalı!',
            'required_with' => ':attribute alanı :values varken zorunludur!'
        ];

        $this->validate($request,$rules,$customMessages);
        
        $diyetisyen = new Diyetisyen;
        $diyetisyen->ad           = $request->input('Name');
        $diyetisyen->soyad        = $request->input('LastName');
        $diyetisyen->adsoyad      = $diyetisyen->ad . " " . $diyetisyen->soyad;
        $diyetisyen->slug         = str_slug($diyetisyen->ad. '-' .$diyetisyen->soyad);
        $diyetisyen->email        = $request->input('Email');
        $diyetisyen->sifre        = $request->input('Şifre');
        $diyetisyen->sifre_tekrar = $request->input('ŞifreTekrar');

        $diyetisyen->save();

        return redirect('/giris');

    }

    public function show($slug)
    {
        $diyetisyen = Diyetisyen::where('slug',$slug)->first();
        if($diyetisyen->onay == 1)
        {
            return view('detay', ['detay' => $diyetisyen]); 
        } else {
            return redirect("/diyetisyens"); 
        }
    }

    public function edit()
    {
        ob_start();
        session_start();

        if(isset($_SESSION['diyetisyen']))
        {
            $email = $_SESSION['diyetisyen'];
            $diyetisyen = DB::table('diyetisyens')->where('email',$email)->first();
            $id = $diyetisyen->id;
            return View('admin-profile')->with('diyetisyen',$diyetisyen)
            ->with('diyetisyen->id',$id);
        }
    }

    public function update(Request $request, $id)
    {
        ob_start();
        session_start();

        $rules = [
            'Ad'        => 'required|alpha',
            'Soyad'    => 'required|alpha',
            'Email'       => 'required|email',
            'Profil' =>     'nullable|image',
            'K_Telefon' => 'nullable|numeric|digits:11',
            'O_Telefon' => 'nullable|numeric|digits:11',
            'Unvan' => 'nullable|string',
            'Hakkimda' => 'nullable|string',
            'Belge' => 'nullable|file',
            'Egitim' => 'nullable|string',
            'Hizmetler' => 'nullable|string',
            'Ucret' => 'nullable|integer|min:10|max:1000',
            'E_Sifre'  => 'nullable|exists:diyetisyens,sifre',
            'Y_Sifre'       => 'nullable|required_with:E_Sifre|min:8',
            'TY_Sifre' => 'required_with:Y_Sifre|same:Y_Sifre'
        ];

        $customMessages = [
            'required' => 'Lütfen :attribute alanını giriniz!',
            'numeric' => 'Lütfen sadece rakam giriniz!',
            'min' => ':attribute alanı en az :min karakter içermelidir!',
            'integer' => ':attribute alanı sayısal değer olmalıdır!',
            'digits' => ':attribute alanı :digits rakam içermelidir!',
            'Ucret.min' => ':attribute alanı en az :min olmalıdır!',
            'Ucret.max' => ':attribute alanı en fazla :max olmalıdır!',
            'image' => ':attribute alanı sadece resim dosyası olabilir!',
            'email' => 'Geçersiz :attribute!',
            'exists' => 'Lütfen geçerli :attribute giriniz!',
            'alpha' => ':attribute alanı sadece harflerden oluşmalı!',
            'same' => ':attribute, şifre ile aynı olmalı!',
            'required_with' => ':attribute alanı :values varken zorunludur!'
        ];

        $this->validate($request,$rules,$customMessages);
        
        if(isset($_SESSION['diyetisyen']))
        {
            $email = $_SESSION['diyetisyen'];
            $user = DB::table('diyetisyens')->where('email',$email)->first();
            $profile_img = $request->file('Profil');
            $belge = $request->file('Belge');

            $diyetisyen = Diyetisyen::find($user->id);
            $diyetisyen->ad = $request->input('Ad'); 
            $diyetisyen->soyad = $request->input('Soyad'); 
            $diyetisyen->adsoyad      = $diyetisyen->ad . " " . $diyetisyen->soyad;
            $diyetisyen->email = $request->input('Email'); 
            $diyetisyen->telefon = $request->input('K_Telefon'); 
            $diyetisyen->ofis_telefon = $request->input('O_Telefon');
            $diyetisyen->egitim = $request->input('Egitim');
            $diyetisyen->hizmetler = $request->input('Hizmetler');
            $diyetisyen->unvan = $request->input('Unvan');
            $diyetisyen->bio = $request->input('Hakkimda');
            $diyetisyen->ucret = $request->input('Ucret');
            $diyetisyen->slug = str_slug($diyetisyen->ad. '-' .$diyetisyen->soyad);

            if(!empty($profile_img)) 
            {
                $diyetisyen->p_foto = $profile_img->getClientOriginalName();
                $resim_hedef = 'uploads';
                $profile_img->move($resim_hedef,$profile_img->getClientOriginalName());
            }

            if(!empty($belge)) 
            {
                $diyetisyen->belge = $belge->getClientOriginalName();
                $resim_hedef = 'uploads';
                $belge->move($resim_hedef,$belge->getClientOriginalName());
            }

            $diyetisyen->save();
            
            return redirect('/admin-profile');
        }
       
    }

    public function destroy($id)
    {
        //
    }


    public function login(Request $request)
    {

        $rules = [
            'Email'  => 'required|email|exists:diyetisyens,email',
            'Şifre'  => 'required|exists:diyetisyens,sifre'
        ];

        $customMessages = [
            'required' => 'Lütfen :attribute giriniz!',
            'email' => 'Geçersiz :attribute!',
            'exists' => 'Girdiğiniz :attribute yanlış!'
        ];

        $this->validate($request,$rules,$customMessages);

        $email = $request->input('Email');
        $sifre = $request->input('Şifre');
        $kullanici = DB::select('select * from diyetisyens where email=? and sifre=?',[$email,$sifre]);
        
        if(count($kullanici))
        {
            ob_start();
            session_start();
            $_SESSION['diyetisyen'] = $email;
            return redirect('/admin-index');
        }
        else
        {
            return redirect('/giris');
        }
  
    }

    public function logout()
    {
        session_start();
        session_destroy();
        return redirect('/giris');
    }

    public function ara(Request $request)
    {
        $aranan = $request->input('Arama'); 
        $sonuclar = Diyetisyen::where(
            [
                ['adsoyad','like', '%'. $aranan . '%'],
                ['onay', '=', 1]
            ]
        )->paginate(5);

        if($aranan != "") {
            return View('diyetisyenler')->with('lists',$sonuclar)->with("search", TRUE)->with("arama", $aranan);
        } else {
            return View('diyetisyenler')->with('lists', Diyetisyen::where(
                [
                    ['onay', '=', 1]
                ]
            )->paginate(5));
        }

        
    }

    public function sirala(Request $request)
    {
        $type = $request->input('orderby');
        if($type == 'puan') {

            $diyetisyenler = Diyetisyen::where('onay', 1)->orderBy("puan", "desc")->paginate(5);

        } elseif($type == 'hizmet') {

            $diyetisyenler = Diyetisyen::where('onay', 1)->orderBy("danisan_sayisi", "desc")->paginate(5);
        }

        return View('diyetisyenler')->with('lists',$diyetisyenler)->with("type", $type);
    }
}
