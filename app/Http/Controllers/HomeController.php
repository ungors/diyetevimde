<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Iletisim;

class HomeController extends Controller
{
    public function index()
    {
        return View('index');
    }

    public function iletisim()
    {
        return View('iletisim');
    }

    public function sss()
    {
        return View('sss');
    }

    public function giris()
    {
        return View('giris');
    }

    public function diyetisyenkayit()
    {
        return View('diyetisyen-kayit');
    }

    public function danisankayit()
    {
        return View('danisan-kayit');
    }

    public function mesajGonder(Request $request)
    {
        $rules = [
            'Ad'        => 'required|alpha',
            'Soyad'    => 'required|alpha',
            'E-mail'       => 'required|email',
            'Telefon'       => 'required|numeric|digits:11',
            'Mesaj' => 'required'
        ];

        $customMessages = [
            'required' => 'Lütfen :attribute alanını giriniz!',
            'email' => 'Geçersiz :attribute!',
            'alpha' => ':attribute alanı sadece harflerden oluşmalı!',
            'digits' => ':attribute alanı :digits rakam içermelidir!'
        ];

        $this->validate($request,$rules,$customMessages);

        $iletisim = new Iletisim;
        $iletisim->ad = $request->input('Ad');
        $iletisim->soyad = $request->input('Soyad');
        $iletisim->email = $request->input('E-mail');
        $iletisim->telefon = $request->input('Telefon');
        $iletisim->mesaj = $request->input('Mesaj');

        $iletisim->save();

        return redirect()->back();
    }

}
