<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Diyetisyen;
use App\Yonetici;
use App\Iletisim;

class YonetimController extends Controller
{
    public function onay()
    {
        $diyetisyenler = Diyetisyen::all();
        return view('yonetim-onay',['diyetisyenler' => $diyetisyenler]);
    }

    public function onayla($id)
    {
        $diyetisyen = Diyetisyen::where('id',$id)->first();

        if($diyetisyen->onay == 0) {

            Diyetisyen::where([
                ["id", "=" ,$id]
            ])->update([
                "onay" => 1
            ]);
    

        } else {

            Diyetisyen::where([
                ["id", "=" ,$id]
            ])->update([
                "onay" => 0
            ]);

        }

        return redirect()->back();
    }

    public function yonetim_mesajlar()
    {
        return view('yonetim-mesajlar',['mesajlar' => Iletisim::all()]);
    }

    public function yonetici_login(Request $request)
    {
        $yonetici = Yonetici::where([
            ['k_adi', '=' ,$request->input('Kullanıcı')],
            ['sifre', '=' ,$request->input('Şifre')]
        ])->first();

            if($yonetici)
            {
                session_start();
                ob_start();
                $_SESSION['yonetici'] = $request->input('Kullanıcı');
                return redirect('yonetim-onay');
            }
            else
            {
                return redirect()->back();
            }
    }

    public function yonetici_logout()
    {
        session_start();
        session_destroy();

        return redirect('yonetici-giris');
    }
}
