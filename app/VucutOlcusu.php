<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VucutOlcusu extends Model
{
    protected $table   = 'vucut_olcusus';
    public $primaryKey = 'id';
    public $timestamps = true;
}
