<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Danisan extends Model
{
    protected $table   = 'danisans';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function blogyorumlar()
    {
        return $this->hasMany('App\BlogYorum','danisan_id','id');
    }

    public function diyetisyenyorumlar()
    {
        return $this->hasMany('App\DiyetisyenYorum','danisan_id','id');
    }

    public function diyetisyen()
    {
        return $this->belongsTo('App\Diyetisyen');
    }
}
