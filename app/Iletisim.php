<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iletisim extends Model
{
    protected $table   = 'iletisims';
    public $primaryKey = 'id';
    public $timestamps = true;
}
