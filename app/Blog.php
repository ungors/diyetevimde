<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table   = 'blogs';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function diyetisyen()
    {
        return $this->belongsTo('App\Diyetisyen');
    }

    public function yorumlar()
    {
        return $this->hasMany('App\BlogYorum','blog_id','id');
    }


}
