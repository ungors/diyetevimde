<?php

Route::get('/', function () {
    return view('index');
});

Route::get('/ödeme',function(){
    return view('ödeme');
});

Route::get('/onay',function(){
    return view('onay');
});

Route::get('/blog-detay',function(){
    return view('blog-detay');
});

Route::get('/detay',function(){
    return view('detay');
});

Route::get('/admin-bookings',function(){
    return view('admin-bookings');
});

Route::get('/blog-olusturma',function(){
    return view('blog-olusturma');
});

Route::get('/admin-profile',function(){
    return view('admin-profile');
});

Route::get('/danisan-giris',function(){
    return view('danisan-giris');
});

Route::get('/yonetici-giris',function(){
    return view('yonetici-giris');
});

Route::get('/danisan-edit',function(){
    return view('danisan-edit');
});

Route::get('/danisan-messages',function(){
    return view('danisan-messages');
});

Route::get('/danisan-olculeri',function(){
    return view('danisan-olculeri');
});

Route::get('/yonetim-onay',function(){
    return view('yonetim-onay');
});


Route::get('/onayla/{id}','YonetimController@onayla');

Route::get('/yonetim-onay','YonetimController@onay');

Route::get('/yonetim-mesajlar','YonetimController@yonetim_mesajlar');

Route::get('/admin-index','DiyetisyenController@adminIndex');

Route::get('/admin-messages','DiyetisyenController@adminMessages');

Route::get('/danisan-messages','DanisanController@danisanMessages');

Route::get('/mesajAl','DiyetisyenController@mesajAl');

Route::get('/admin-profile','DiyetisyenController@edit');

Route::get('/danisan-edit','DanisanController@edit');

Route::get('/cikis','DiyetisyenController@logout');

Route::get('/danisan_cikis','DanisanController@danisan_logout');

Route::get('/yonetici_cikis','YonetimController@yonetici_logout');

Route::get('/danisan-profile','DanisanController@danisanProfile');

Route::post('/login','DiyetisyenController@login');

Route::post('/mesajGonder','HomeController@mesajGonder');

Route::post('/yonetici_login','YonetimController@yonetici_login');

Route::post('/olcuduzenle','DanisanController@olcuduzenle');

Route::post('/sirala','DiyetisyenController@sirala');

Route::post('/danisan_login','DanisanController@danisan_login');

Route::resource('diyetisyens','DiyetisyenController');

Route::resource('danisans','DanisanController');

Route::resource('blogs','BlogController');

Route::resource('yorums','YorumController');

Route::resource('odemes','OdemeController');

Route::get('admin-blogs','BlogController@adminBlogs');

Route::post('blog-ara','BlogController@ara');

Route::post('diyetisyen-ara','DiyetisyenController@ara');

Route::get('index','HomeController@index');

Route::get('iletisim','HomeController@iletisim');

Route::get('sss','HomeController@sss');

Route::get('giris','HomeController@giris');

Route::get('diyetisyen-kayit','HomeController@diyetisyenkayit');

Route::get('danisan-kayit','HomeController@danisankayit');

Route::resource('messages','MessageController');