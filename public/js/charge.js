Stripe.setPublishableKey('pk_test_Wizwiwn1oARUztvY6rccackr');

var $form = $('#payment-form');

$form.submit(function(event)
{
	Stripe.card.createToken({
		number : $('#card_number').val(),
		exp_month : $('#expire_month').val(),
		exp_year : $('#expire_year').val(),
		ccv : $('#ccv').val()
	},stripeResponseHandler);
	return false;
});

function stripeResponseHandler(status,response)
{
	if(response.error)
	{
		$('#charge-error').removeAttr('visibility','hidden');
		$('#charge-error').text(response.error.message);
	}
	else
	{
		var token = response.id;
		$form.append($('<input type="hidden" name="stripeToken" />').val(token));

		$form.get(0).submit();
	}
}

