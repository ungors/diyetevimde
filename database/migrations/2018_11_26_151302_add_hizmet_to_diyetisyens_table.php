<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHizmetToDiyetisyensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diyetisyens', function (Blueprint $table) {
            $table->text('hizmetler');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diyetisyens', function (Blueprint $table) {
            $table->dropColumn('hizmetler');
        });
    }
}
