<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiyetisyensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diyetisyens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ad');
            $table->string('soyad');
            $table->string('email');
            $table->text('bio');
            $table->string('telefon');
            $table->string('ofis_telefon');
            $table->string('p_foto',200);
            $table->float('puan');
            $table->string('unvan');
            $table->integer('egitim_id');
            $table->string('belge',200);
            $table->string('cinsiyet');
            $table->string('sifre');
            $table->string('sifre_tekrar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diyetisyens');
    }
}
