<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDanisanSayisiToDiyetisyensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diyetisyens', function (Blueprint $table) {
            $table->integer('danisan_sayisi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diyetisyens', function (Blueprint $table) {
            $table->dropColumn('danisan_sayisi');
        });
    }
}
