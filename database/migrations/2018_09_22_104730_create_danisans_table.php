<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDanisansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('danisans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ad');
            $table->string('soyad');
            $table->string('email');
            $table->string('telefon');
            $table->string('p_foto',200);
            $table->string('cinsiyet');
            $table->integer('diyetisyen_id');
            $table->string('sifre');
            $table->string('sifre_tekrar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('danisans');
    }
}
