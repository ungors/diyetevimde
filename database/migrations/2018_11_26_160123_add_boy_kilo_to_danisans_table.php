<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBoyKiloToDanisansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('danisans', function (Blueprint $table) {
            $table->integer('boy');
            $table->float('kilo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('danisans', function (Blueprint $table) {
            $table->dropColumn('boy');
            $table->dropColumn('kilo');
        });
    }
}
